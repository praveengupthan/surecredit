<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sure Credit</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/fav.png"> 
    <?php include 'includes/styles.php'?>
    <?php include 'includes/data.php'?>
</head>

<body> <?php include'includes/headerPostLogin.php' ?>
    <!-- main -->
    <main class="subpageMain profileSubPage">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <?php include 'includes/userLeftNav.php'?>
                <!-- right section -->
                <div class="col-lg-8">
                    <div class="shadowBox">
                        <!-- content -->
                        <div class="profileContent p-2 p-lg-4">
                            <!-- row -->
                            <div class="row mb-4">
                                <div class="col-md-12">
                                    <h5 class="flight h5 border-bottom mb-3 pb-3"><span class="fbold fblue">4000CD01505084</span></h5>
                                </div>                              
                            </div>
                            <!--/ row -->

                            <!-- tab -->
                            <div class="Customtabs">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link active" id="loanDetails-tab" data-bs-toggle="tab" data-bs-target="#loanDetails" type="button" role="tab" aria-controls="loanDetails" aria-selected="true">Loan Details</button>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link" id="chargesDetails-tab" data-bs-toggle="tab" data-bs-target="#chargesDetails" type="button" role="tab" aria-controls="chargesDetails" aria-selected="false">Charge Details</button>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link" id="statements-tab" data-bs-toggle="tab" data-bs-target="#statements" type="button" role="tab" aria-controls="statements" aria-selected="false">Statements</button>
                                    </li>
                                </ul>
                                <div class="tab-content" id="myTabContent">
                                     <div class="tab-pane fade show active" id="loanDetails" role="tabpanel" aria-labelledby="loanDetails-tab">
                                          <!-- loan Details -->
                                          <div class="row">
                                                <!-- col -->
                                                <div class="col-md-6">
                                                    <div class="d-flex pillItem">
                                                        <span>Product</span>
                                                        <span class="seperator">:</span>
                                                        <span class="fbold">Personal Loan</span>
                                                    </div>
                                                </div>
                                                <!-- / col -->
                                                <!-- col -->
                                                <div class="col-md-6">
                                                    <div class="d-flex pillItem">
                                                        <span>Linked Account</span>
                                                        <span class="seperator">:</span>
                                                        <span class="fbold"><a href="javascript:void(0)" class="fbold fgreen text-uppercase">View</a></span>
                                                    </div>
                                                </div>
                                                <!-- / col -->
                                                <!-- col -->
                                                <div class="col-md-6">
                                                    <div class="d-flex pillItem">
                                                        <span>Loan Account </span>
                                                        <span class="seperator">:</span>
                                                        <span class="fbold">4000CD01505084</span>
                                                    </div>
                                                </div>
                                                <!-- / col -->
                                                <!-- col -->
                                                <div class="col-md-6">
                                                    <div class="d-flex pillItem">
                                                        <span>Annual Rate of %</span>
                                                        <span class="seperator">:</span>
                                                        <span class="fbold">10.6%</span>
                                                    </div>
                                                </div>
                                                <!-- / col -->
                                                <!-- col -->
                                                <div class="col-md-6">
                                                    <div class="d-flex pillItem">
                                                        <span>Total Loan Amount</span>
                                                        <span class="seperator">:</span>
                                                        <span class="fbold">Rs. 96,000</span>
                                                    </div>
                                                </div>
                                                <!-- / col -->
                                                <!-- col -->
                                                <div class="col-md-6">
                                                    <div class="d-flex pillItem">
                                                        <span>EMI Amount</span>
                                                        <span class="seperator">:</span>
                                                        <span class="fbold">Rs. 5,500</span>
                                                    </div>
                                                </div>
                                                <!-- / col -->
                                                <!-- col -->
                                                <div class="col-md-6">
                                                    <div class="d-flex pillItem">
                                                        <span>Principal Out Standing</span>
                                                        <span class="seperator">:</span>
                                                        <span class="fbold">Rs. 10,000</span>
                                                    </div>
                                                </div>
                                                <!-- / col -->
                                                 <!-- col -->
                                                <div class="col-md-6">
                                                    <div class="d-flex pillItem">
                                                        <span>Total  Repaid</span>
                                                        <span class="seperator">:</span>
                                                        <span class="fbold">Rs. 74,500</span>
                                                    </div>
                                                </div>
                                                <!-- / col -->
                                          </div>
                                          <!--/ loan Details -->

                                          <!-- loan schedule -->
                                          <div class="loanTitle py-3 border-top border-bottom my-2">
                                              <h5 class="h5 fbold mb-0 pb-0">Loan Schedule</h5>
                                          </div>
                                          <!-- row -->
                                          <div class="row">
                                                <!-- col -->
                                                <div class="col-md-6">
                                                    <div class="d-flex pillItem">
                                                        <span>1 st EMI Due Date</span>
                                                        <span class="seperator">:</span>
                                                        <span class="fbold">05-03-2014</span>
                                                    </div>
                                                </div>
                                                <!-- / col -->
                                                <!-- col -->
                                                <div class="col-md-6">
                                                    <div class="d-flex pillItem">
                                                        <span>Last EMI Date</span>
                                                        <span class="seperator">:</span>
                                                        <span class="fbold">05-03-2014</span>
                                                    </div>
                                                </div>
                                                <!-- / col -->
                                            </div>
                                            <!-- row -->
                                            <!-- row -->
                                          <div class="row">
                                                <div class="col-md-12">
                                                    <div class="barPayment">
                                                        <div class="barin">
                                                            <div class="barinn" style="width:70%"></div>
                                                        </div>
                                                        <p class="d-flex justify-content-between py-2">
                                                            <span class="fbold">Total EMI's 24</span>
                                                            <span class="fbold">19 EMI's Paid</span>
                                                            <span class="fbold">5 EMI's Pending</span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- row -->
                                            <!-- row -->
                                            <div class="row">
                                                <!-- col -->
                                                <div class="col-md-6">
                                                    <div class="d-flex pillItem">
                                                        <span>Next EMI Due Date</span>
                                                        <span class="seperator">:</span>
                                                        <span class="fbold">24-05-2022</span>
                                                    </div>
                                                </div>
                                                <!-- / col -->
                                                <!-- col -->
                                                <div class="col-md-6">
                                                    <div class="d-flex pillItem">
                                                        <span>Previous Payment Date</span>
                                                        <span class="seperator">:</span>
                                                        <span class="fbold">05-01-2022</span>
                                                    </div>
                                                </div>
                                                <!-- / col -->
                                            </div>
                                            <!-- row -->
                                          <!--/ loan schedule-->

                                          <!-- loan Repayment -->
                                          <div class="loanTitle py-3 border-top border-bottom my-2">
                                              <h5 class="h5 fbold mb-0 pb-0">Loan Repayment</h5>
                                          </div>
                                          <!-- row -->
                                          <div class="row">
                                              <div class="col-md-12">
                                                  <!-- chart -->
                                                  <div>
                                                      <img src="img/barimg.png" class="img-fluid" alt="">
                                                  </div>
                                                  <!--/ chart -->
                                              </div>
                                          </div>
                                          <!--/ row -->
                                          <!-- row -->
                                            <div class="row">
                                                <!-- col -->
                                                <div class="col-md-6">
                                                    <div class="d-flex pillItem">
                                                        <span>Total EMI Amount Repaid</span>
                                                        <span class="seperator">:</span>
                                                        <span class="fbold">Rs. 74,664</span>
                                                    </div>
                                                </div>
                                                <!-- / col -->
                                                <!-- col -->
                                                <div class="col-md-6">
                                                    <div class="d-flex pillItem">
                                                        <span>Outstanding Interest Amount</span>
                                                        <span class="seperator">:</span>
                                                        <span class="fbold">Rs. 0</span>
                                                    </div>
                                                </div>
                                                <!-- / col -->
                                                <!-- col -->
                                                <div class="col-md-6">
                                                    <div class="d-flex pillItem">
                                                        <span>EMI Amount Advance Paid</span>
                                                        <span class="seperator">:</span>
                                                        <span class="fbold">Rs. 21,336</span>
                                                    </div>
                                                </div>
                                                <!-- / col -->
                                            </div>
                                            <!-- row -->
                                          <!--/ loan repayment -->
                                          <!-- payment info-->
                                          <div class="loanTitle py-3 border-top border-bottom my-2">
                                              <h5 class="h5 fbold mb-0 pb-0">Payment Info></h5>
                                          </div>
                                          <!--/ payment info -->
                                          <!-- row -->
                                            <div class="row">
                                                <!-- col -->
                                                <div class="col-md-6">
                                                    <div class="d-flex pillItem">
                                                        <span>Repayment Frequently</span>
                                                        <span class="seperator">:</span>
                                                        <span class="fbold">Monthly</span>
                                                    </div>
                                                </div>
                                                <!-- / col -->
                                                <!-- col -->
                                                <div class="col-md-6">
                                                    <div class="d-flex pillItem">
                                                        <span>Payment Method</span>
                                                        <span class="seperator">:</span>
                                                        <span class="fbold">Auto Debit</span>
                                                    </div>
                                                </div>
                                                <!-- / col -->
                                                <!-- col -->
                                                <div class="col-md-6">
                                                    <div class="d-flex pillItem">
                                                        <span>Payment Source</span>
                                                        <span class="seperator">:</span>
                                                        <span class="fbold">SBI, Ferozguda, Bowenpally</span>
                                                    </div>
                                                </div>
                                                <!-- / col -->
                                            </div>
                                            <!-- row -->
                                     </div>
                                     <div class="tab-pane fade" id="chargesDetails" role="tabpanel" aria-labelledby="chargesDetails-tab">
                                          <!-- charges details -->
                                           <!-- row -->
                                            <div class="row">
                                                <!-- col -->
                                                <div class="col-md-6">
                                                    <div class="d-flex pillItem">
                                                        <span>Rate of Interest (ROI)</span>
                                                        <span class="seperator">:</span>
                                                        <span class="fbold">7.5% PA</span>
                                                    </div>
                                                </div>
                                                <!-- / col -->
                                                <!-- col -->
                                                <div class="col-md-6">
                                                    <div class="d-flex pillItem">
                                                        <span>Total Interest Paid</span>
                                                        <span class="seperator">:</span>
                                                        <span class="fbold">Rs: 25,000</span>
                                                    </div>
                                                </div>
                                                <!-- / col -->
                                                <!-- col -->
                                                <div class="col-md-6">
                                                    <div class="d-flex pillItem">
                                                        <span>Monthly Interest</span>
                                                        <span class="seperator">:</span>
                                                        <span class="fbold">Rs: 1200</span>
                                                    </div>
                                                </div>
                                                <!-- / col -->
                                            </div>
                                            <!-- row -->
                                          <!-- charges details -->
                                     </div>
                                     <div class="tab-pane fade" id="statements" role="tabpanel" aria-labelledby="statements-tab">
                                        <!-- statements -->
                                        <div class="row">
                                            <!-- col -->
                                            <div class="col-md-6">
                                                <div class="downloadItem d-flex justify-content-between">
                                                    <div class="align-self-center">
                                                        <span class="icon-file-o icomoon h4"></span>
                                                        <span class="d-inline-block ps-3 titlename">Statement of Account</span>
                                                    </div>
                                                    <a href="javascript:void(0)" class="downIcon"><span class="icon-download01 icomoon"></span></a>
                                                </div>
                                            </div>
                                            <!--/ col -->
                                             <!-- col -->
                                            <div class="col-md-6">
                                                <div class="downloadItem d-flex justify-content-between">
                                                    <div class="align-self-center">
                                                        <span class="icon-file-o icomoon h4"></span>
                                                        <span class="d-inline-block ps-3 titlename">Repayment Schedule</span>
                                                    </div>
                                                    <a href="javascript:void(0)" class="downIcon"><span class="icon-download01 icomoon"></span></a>
                                                </div>
                                            </div>
                                            <!--/ col -->
                                             <!-- col -->
                                            <div class="col-md-6">
                                                <div class="downloadItem d-flex justify-content-between">
                                                    <div class="align-self-center">
                                                        <span class="icon-file-o icomoon h4"></span>
                                                        <span class="d-inline-block ps-3 titlename">No Object Certificate (NOC)</span>
                                                    </div>
                                                    <a href="javascript:void(0)" class="downIcon"><span class="icon-download01 icomoon"></span></a>
                                                </div>
                                            </div>
                                            <!--/ col -->
                                             <!-- col -->
                                            <div class="col-md-6">
                                                <div class="downloadItem d-flex justify-content-between">
                                                    <div class="align-self-center">
                                                        <span class="icon-file-o icomoon h4"></span>
                                                        <span class="d-inline-block ps-3 titlename">Interest Certificate</span>
                                                    </div>
                                                    <a href="javascript:void(0)" class="downIcon"><span class="icon-download01 icomoon"></span></a>
                                                </div>
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <!--/ statement s-->
                                     </div>
                                </div>
                            </div>
                            <!--/ tab -->
                           
                        </div>
                        <!--/ content -->
                    </div>
                </div>
                <!--/ right section -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->

    </main>
    <!--/ main --> 
    <?php include 'includes/footer.php' ?> 
    <?php include 'includes/scripts.php'?>
</body>

</html>