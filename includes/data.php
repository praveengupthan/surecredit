<?php
 //home process columns

 $processItem = array(
    array(
        "icon-register",
        "1",
        "Register",
        "Sign up with your mobile number, fill up basic details"        
    ),
     array(
        "icon-uploaddocuments",
        "2",
        "Upload Documents",
        "We don’t need your physical documents or presence. Our entire process is based online. We need your KYC and bank-related documents that you would need to upload on our app. "        
    ),
     array(
        "icon-check",
        "3",
        "Apply",
        "Fill in a few details"        
    ),
     array(
        "icon-fasttime",
        "4",
        "Instant Approval",
        "Once our experts have your eligibility, we will instantly approve your loan. The process is as simple as that! "        
    ),
     array(
        "icon-banktransfer",
        "5",
        "Transfer Money",
        "One click transfer to your bank "        
    ),
 );

 $whychooseItem = array(
     array(
        "icon-verify",
        "Easy Eligibility Criteria",       
     ),
     array(
        "icon-documents",
        "Limited Documentation",       
     ),
     array(
        "icon-fasttime",
        "Quick Loan Disbursal",        
     ),
     array(
        "icon-borrow02",
        "Borrow flexibly",        
     ),
       array(
        "icon-interest",
        "Flexible re-payment",       
     ),
     array(
        "icon-handshake",
        "Friendly banking experience",       
     ),
     array(
        "icon-interest",
        "No usage, no interest",        
     ),
     array(
        "icon-lowinterest",
        "Best Industry-wide Interest rate",        
     ),
       array(
        "icon-safecreditcard",
        "Safe & Secure Credit",       
     ),
     array(
        "icon-onlinefile",
        "100% Paperless",       
     ),
     array(
        "icon-guaranteer",
        "No collateral, no guarantors",        
     )  
);

$productItem = array(
   array(
      "product01.jpg",
      "Instant Personal Loan",
      "Customized personal loan with real flexibility benefits. SureCredit offers innovative, smart flexible personal loans that understand your life stage financial gaps. Apply now and the loan is credited into your bank account on the same day."
   ),
    array(
      "salaryadvanceloan.jpg",
      "Salary Advance Loan",
      "At SureCredit, the salary advance loans have been specially designed for the working professionals to help them fulfil financial needs. Download the app now to avail best features. "
   ),
    array(
      "travelloan.jpg",
      "Travel Loan",
      "If you wish to make your vacation one to remember, then our personal loans for travel are perfect for you!"
   )
);

$howItem = array (
   array(
      "icon-money",
      "Flexible Loan Amount",
      "For cash emergencies, borrow cash amounts from ` 5000 – ` 1,50,000 within minutes"      
   ),
    array(
      "icon-interest",
      "No usage, no interest",
      "Interest rates are applied only on the amount you borrow and not your entire approved limit."      
   ),
    array(
      "icon-process",
      "Transparent Processes",
      "No hidden charges, No foreclosure charges and robust customer support"      
   ),
    array(
      "icon-transfermoney",
      "Flexible Repayment Plan",
      "Accelerated payment option or bullet repayment to principal outstanding. We offer personal loan the way you like it"      
   ),
    array(
      "icon-verification2",
      "Paperless process",
      "At SureCredit instant loan application process does not require you to physically submit documents whole process right from the beginning is online. Even your document verification is done online. "      
   ),
    array(
      "icon-wallet",
      "Pay For What You Use",
      "At SureCredit you just have to pay for what service you are using"      
   ),
    array(
      "icon-guaranteers",
      "No collateral, no guarantors",
      "SureCredit's credit line app lets you access money when you need it the most without any collateral backing! When compared to others, do not take any hidden charges when your loan is processed. "      
   ),
    array(
      "icon-account02",
      "Individual Account Manager",
      "Every customer is value to us therefore we have dedicated Relationship Manger to every individual."      
   ),
);

$userdbItem = array(
   array(
      "CURRENT BALANCE",
      "icon-loan",
      "Rs:1,81,766.00",
      "javascript:void(0)"
   ),
   array(
      "AMOUNT OVER DUE",
      "icon-duedate",
      "Rs:1,81,766.00",
      "javascript:void(0)"
   ),
   array(
      "TOTAL ACCOUNTS",
      "icon-account02",
      "Rs:1,81,766.00",
      "javascript:void(0)"
   ),
   array(
      "TOTAL REWARD POINTS",
      "icon-award",
      "Rs:1,81,766.00",
      "javascript:void(0)"
   ),
);

$loanItem = array(
   array(
      "PERSONAL LOAN",
      "userLoanDetail.php",
      "Loan Amount (Rs)",
      "1,25,000.00",
      "Tenor in months",
      "14/14",
      "Next EMI date",
      "01-10-2021",
      "Loan Ending Date",
      "01-10-2021"
   ),
   array(
      "PERSONAL LOAN",
      "userLoanDetail.php",
      "Loan Amount (Rs)",
      "1,25,000.00",
      "Tenor in months",
      "14/14",
      "Next EMI date",
      "01-10-2021",
      "Loan Ending Date",
      "01-10-2021"
   ),
   array(
      "PERSONAL LOAN",
      "userLoanDetail.php",
      "Loan Amount (Rs)",
      "1,25,000.00",
      "Tenor in months",
      "14/14",
      "Next EMI date",
      "01-10-2021",
      "Loan Ending Date",
      "01-10-2021"
   ),
   array(
      "PERSONAL LOAN",
      "userLoanDetail.php",
      "Loan Amount (Rs)",
      "1,25,000.00",
      "Tenor in months",
      "14/14",
      "Next EMI date",
      "01-10-2021",
      "Loan Ending Date",
      "01-10-2021"
   ),
);



?>