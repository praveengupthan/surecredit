<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sure Credit Privacy</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/fav.png"> 
    <?php include 'includes/styles.php'?>
    <?php include 'includes/data.php'?>
</head>

<body> <?php include'includes/header.php' ?>
    <!-- main -->
    <main class="subpageMain">
        <!-- subpage body -->
        <section class="subpageBody">           
            <!-- sub page Body Content -->
            <div class="applypage">
                <div class="container">
                    <div class="row py-3 py-lg-5">                       
                        <div class="col-md-12">
                             <!-- tab -->
                            <div class="Customtabs">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link active" id="personalDetails-tab" data-bs-toggle="tab" data-bs-target="#personalDetails" type="button" role="tab" aria-controls="personalDetails" aria-selected="true">Personal Info</button>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link" id="professionalInfo-tab" data-bs-toggle="tab" data-bs-target="#professionalInfo" type="button" role="tab" aria-controls="professionalInfo" aria-selected="false">Professional Info</button>
                                    </li>   
                                     <li class="nav-item" role="presentation">
                                        <button class="nav-link" id="documents-tab" data-bs-toggle="tab" data-bs-target="#documents" type="button" role="tab" aria-controls="documents" aria-selected="false">Documents</button>
                                    </li> 
                                     <li class="nav-item" role="presentation">
                                        <button class="nav-link" id="bankInfo-tab" data-bs-toggle="tab" data-bs-target="#bankInfo" type="button" role="tab" aria-controls="bankInfo" aria-selected="false">Bank  Info</button>
                                    </li> 
                                     <li class="nav-item" role="presentation">
                                        <button class="nav-link" id="references-tab" data-bs-toggle="tab" data-bs-target="#references" type="button" role="tab" aria-controls="references" aria-selected="false">References</button>
                                    </li>                                
                                </ul>
                                <div class="tab-content" id="myTabContent">
                                     <div class="tab-pane fade show active" id="personalDetails" role="tabpanel" aria-labelledby=""personalDetails-tab">
                                          <!-- Personal Deails -->
                                          <div class="row">
                                                <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <input type="text" class="form-control is-invalid" id="Name" placeholder="Enter Name">
                                                         <div class="invalid-feedback">
                                                             Please choose a username.
                                                        </div>
                                                        <label for="Name">Name <span class="text-danger">*</span></label>
                                                    </div>
                                                    
                                                </div>
                                                <!-- / col -->   
                                                 <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <select class="form-select"  id="SelectGender" aria-label="Select Gender">
                                                            <option>Male</option>
                                                            <option>Fe-Male</option>
                                                        </select>
                                                         <label for="SelectGender">Select Gender <span class="text-danger">*</span> </label>
                                                    </div>
                                                </div>
                                                <!-- / col -->    
                                                 <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <input type="email" class="form-control" id="Email" placeholder="Enter Email">
                                                        <label for="Email">Email <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <!-- / col -->        
                                                <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <input type="email" class="form-control" id="AlternativeEmail" placeholder="Enter Alternative Email">
                                                        <label for="AlternativeEmail">Alternative Email</label>
                                                    </div>
                                                </div>
                                                <!-- / col -->             
                                                <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <input type="text" class="form-control" id="MobileNumber" placeholder="Mobile Number">
                                                        <label for="MobileNumber">Mobile Number <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <!-- / col -->                                     
                                                <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <input type="text" class="form-control" id="AlternativeMobileNumber" placeholder="Alternative Mobile Number">
                                                        <label for="AlternativeMobileNumber">Alternative Mobile</label>
                                                    </div>
                                                </div>
                                                <!-- / col --> 
                                                 <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <input type="date" class="form-control" id="DateofBirth" placeholder="Date of Birth">
                                                        <label for="DateofBirth">Date of Birth <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <!-- / col --> 
                                                 <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <select class="form-select"  id="NativeCity" aria-label="Native City">
                                                            <option>Hyderabad</option>
                                                            <option>Secundearbad</option>
                                                        </select>
                                                         <label for="NativeCity">Native City <span class="text-danger">*</span> </label>
                                                    </div>
                                                </div>
                                                <!-- / col -->   
                                                 <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <input type="text" class="form-control" id="AadharNumber" placeholder="Aadhar Number">
                                                        <label for="AadharNumber">Aadhar Number <span class="text-danger">*</span> </label>
                                                    </div>
                                                </div>
                                                <!-- / col -->  
                                                 <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <input type="text" class="form-control" id="PANNumber" placeholder="PAN Number">
                                                        <label for="PANNumber">PAN Number <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <!-- / col --> 
                                                 <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <select class="form-select"  id="MaritalStatus" aria-label="Marital Status">
                                                            <option>Married</option>
                                                            <option>Un-Married</option>
                                                        </select>
                                                         <label for="MaritalStatus">Marital Status <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <!-- / col -->  
                                                 <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <select class="form-select"  id="ResidentType" aria-label="Resident Type">
                                                            <option>Own House</option>
                                                            <option>Rented with Family</option>
                                                        </select>
                                                         <label for="ResidentType">Resident Type <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <!-- / col -->   
                                                 <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <input type="text" class="form-control" id="FathersName" placeholder="Fathers Name">
                                                        <label for="FathersName">Fathers Name <span class="text-danger">*</span> </label>
                                                    </div>
                                                </div>
                                                <!-- / col --> 

                                                <!-- col -->
                                                <div class="col-md-12">
                                                    <h5 class="h5 fbold">Present Address</h5>
                                                </div>
                                                <!--/ col -->
                                                <!-- col -->
                                                <div class="col-md-12 col-lg-9">
                                                    <div class="form-floating mb-3">
                                                        <input type="text" class="form-control" id="PresentAddress" placeholder="Present Address">
                                                        <label for="PresentAddress">Present Address <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <!-- / col --> 
                                                 <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <select class="form-select"  id="City" aria-label="City">
                                                            <option>Hyderabad</option>
                                                            <option>Secundearbad</option>
                                                        </select>
                                                         <label for="City">City <span class="text-danger">*</span> </label>
                                                    </div>
                                                </div>
                                                <!-- / col -->   
                                                 <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <select class="form-select"  id="District" aria-label="District">
                                                            <option>Hyderabad</option>
                                                            <option>Rangareddy</option>
                                                        </select>
                                                         <label for="District">District <span class="text-danger">*</span> </label>
                                                    </div>
                                                </div>
                                                <!-- / col -->   
                                                 <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <select class="form-select"  id="State" aria-label="State">
                                                            <option>Telangana</option>
                                                            <option>Andhra Pradesh</option>
                                                        </select>
                                                         <label for="State">State <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <!-- / col -->   
                                                 <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <input type="text" class="form-control" id="PinCode" placeholder="Pin Code">
                                                        <label for="PinCode">Pin Code <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <!-- / col --> 
                                                 <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <input type="text" class="form-control" id="LandMark" placeholder="Land Mark">
                                                        <label for="LandMark">Land Mark</label>
                                                    </div>
                                                </div>
                                                <!-- / col --> 
                                                 <!-- col -->
                                                <div class="col-md-12">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" value="" id="presentAddressas">
                                                        <label class="form-check-label" for="presentAddressas">
                                                            Perminent Address same as Present Address
                                                        </label>
                                                    </div>
                                                </div>
                                                <!--/ col -->                                              
                                                    <!-- col -->
                                                    <div class="col-md-12">
                                                        <h5 class="h5 fbold">Perminent Address</h5>
                                                    </div>
                                                    <!--/ col -->
                                                    <!-- col -->
                                                    <div class="col-md-12 col-lg-9">
                                                        <div class="form-floating mb-3">
                                                            <input type="text" class="form-control" id="PresentAddress" placeholder="Present Address">
                                                            <label for="PresentAddress">Present Address <span class="text-danger">*</span></label>
                                                        </div>
                                                    </div>
                                                    <!-- / col --> 
                                                    <!-- col -->
                                                    <div class="col-md-6 col-lg-3">
                                                        <div class="form-floating mb-3">
                                                            <select class="form-select"  id="City" aria-label="City">
                                                                <option>Hyderabad</option>
                                                                <option>Secundearbad</option>
                                                            </select>
                                                            <label for="City">City <span class="text-danger">*</span> </label>
                                                        </div>
                                                    </div>
                                                    <!-- / col -->   
                                                    <!-- col -->
                                                    <div class="col-md-6 col-lg-3">
                                                        <div class="form-floating mb-3">
                                                            <select class="form-select"  id="District" aria-label="District">
                                                                <option>Hyderabad</option>
                                                                <option>Rangareddy</option>
                                                            </select>
                                                            <label for="District">District <span class="text-danger">*</span> </label>
                                                        </div>
                                                    </div>
                                                    <!-- / col -->   
                                                    <!-- col -->
                                                    <div class="col-md-6 col-lg-3">
                                                        <div class="form-floating mb-3">
                                                            <select class="form-select"  id="State" aria-label="State">
                                                                <option>Telangana</option>
                                                                <option>Andhra Pradesh</option>
                                                            </select>
                                                            <label for="State">State <span class="text-danger">*</span></label>
                                                        </div>
                                                    </div>
                                                    <!-- / col -->   
                                                    <!-- col -->
                                                    <div class="col-md-6 col-lg-3">
                                                        <div class="form-floating mb-3">
                                                            <input type="text" class="form-control" id="PinCode" placeholder="Pin Code">
                                                            <label for="PinCode">Pin Code <span class="text-danger">*</span></label>
                                                        </div>
                                                    </div>
                                                    <!-- / col --> 
                                                    <!-- col -->
                                                    <div class="col-md-6 col-lg-3">
                                                        <div class="form-floating mb-3">
                                                            <input type="text" class="form-control" id="LandMark" placeholder="Land Mark">
                                                            <label for="LandMark">Land Mark</label>
                                                        </div>
                                                    </div>
                                                    <!-- / col -->                                                                                                
                                                
                                            </div>
                                            <!-- row -->
                                          <!--/ Personal Deails-->
                                           <!-- col -->
                                            <div class="col-md-12 text-end">
                                                 <button class="btnCustom" type="submit">Next</button>
                                            </div>
                                            <!--/ col -->
                                     </div>

                                     <div class="tab-pane fade" id="professionalInfo" role="tabpanel" aria-labelledby="professionalInfo-tab">
                                          <!-- Professional details -->
                                           <!-- row -->
                                            <div class="row">
                                               <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <input type="text" class="form-control" id="CompanyName" placeholder="Company Name">
                                                        <label for="CompanyName">Company Name <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <!-- / col --> 

                                                <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <input type="text" class="form-control" id="CompanyURL" placeholder="Enter Company URL">
                                                        <label for="CompanyURL">Company URL </label>
                                                    </div>
                                                </div>
                                                <!-- / col --> 

                                                <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <select class="form-select"  id="IndustryType" aria-label="Industry Type">
                                                            <option>Software</option>
                                                            <option>Education</option>
                                                        </select>
                                                        <label for="IndustryType">Industry Type <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <!-- / col -->   

                                                <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <select class="form-select"  id="EmploymentType" aria-label="Employment Type">
                                                            <option>Part Time</option>
                                                            <option>Contract</option>
                                                        </select>
                                                        <label for="EmploymentType">Employment Type <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <!-- / col -->

                                                <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <select class="form-select"  id="Department" aria-label="Department">
                                                            <option>Technology</option>
                                                            <option>Human Resource</option>
                                                        </select>
                                                        <label for="Department">Department <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <!-- / col -->

                                                 <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <select class="form-select"  id="Designation" aria-label="Designation">
                                                            <option>Software Engineer</option>
                                                            <option>Accountant</option>
                                                        </select>
                                                        <label for="Designation">Designation <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <!-- / col -->

                                                <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <select class="form-select"  id="ModeofSalary" aria-label="Mode of Salary">
                                                            <option>Bank Transfer</option>
                                                            <option>Cheque</option>
                                                        </select>
                                                        <label for="ModeofSalary">Mode of Salary <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <!-- / col -->

                                                 <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <input type="text" class="form-control" id="MonthlyNetSalary" placeholder="Monthly Net Salary">
                                                        <label for="MonthlyNetSalary">Monthly Net Salary <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <!-- / col --> 

                                                 <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <select class="form-select"  id="ExperienceinMonths" aria-label="Experience in Months">
                                                            <option>6-12</option>
                                                            <option>12-24</option>
                                                        </select>
                                                        <label for="ExperienceinMonths">Experience in Months <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <!-- / col -->

                                                 <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <select class="form-select"  id="HighestQualifications" aria-label="Highest Qualifications">
                                                            <option>UG / Graduation</option>
                                                            <option>Post Graduation</option>
                                                        </select>
                                                        <label for="HighestQualifications">Highest Qualifications <span class="text-danger">*</span> </label>
                                                    </div>
                                                </div>
                                                <!-- / col -->

                                                 <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <select class="form-select"  id="PassedinYear" aria-label="Passed in Year">
                                                            <option>1996</option>
                                                            <option>1997</option>
                                                        </select>
                                                        <label for="PassedinYear">Passed in Year <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <!-- / col -->

                                                 <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <select class="form-select"  id="Jobcity" aria-label="Job city">
                                                            <option>Hyderabad</option>
                                                            <option>Secundearbad</option>
                                                        </select>
                                                        <label for="Jobcity">Job city <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <!-- / col -->

                                                 <!-- col -->
                                                <div class="col-md-12">
                                                    <h5 class="h5 fbold">Office Address</h5>
                                                </div>
                                                <!--/ col -->

                                                  <!-- col -->
                                                    <div class="col-md-12 col-lg-9">
                                                        <div class="form-floating mb-3">
                                                            <input type="text" class="form-control" id="OfficeAddress" placeholder="Office Address">
                                                            <label for="OfficeAddress">Office Address <span class="text-danger">*</span></label>
                                                        </div>
                                                    </div>
                                                    <!-- / col --> 
                                                    <!-- col -->
                                                    <div class="col-md-6 col-lg-3">
                                                        <div class="form-floating mb-3">
                                                            <select class="form-select"  id="State" aria-label="State">
                                                                <option>Telangana</option>
                                                                <option>Andhra Pradesh</option>
                                                            </select>
                                                            <label for="State">State <span class="text-danger">*</span></label>
                                                        </div>
                                                    </div>
                                                    <!-- / col -->   
                                                   
                                                    <!-- col -->
                                                    <div class="col-md-6 col-lg-3">
                                                        <div class="form-floating mb-3">
                                                            <select class="form-select"  id="City" aria-label="City">
                                                                <option>Hyderabad</option>
                                                                <option>Secundearbad</option>
                                                            </select>
                                                            <label for="City">City / Town <span class="text-danger">*</span></label>
                                                        </div>
                                                    </div>
                                                    <!-- / col -->   
                                                    <!-- col -->
                                                    <div class="col-md-6 col-lg-3">
                                                        <div class="form-floating mb-3">
                                                            <input type="text" class="form-control" id="PinCode" placeholder="Pin Code">
                                                            <label for="PinCode">Pin Code <span class="text-danger">*</span></label>
                                                        </div>
                                                    </div>
                                                    <!-- / col --> 
                                                     <!-- col -->
                                                    <div class="col-md-6 col-lg-3">
                                                        <div class="form-floating mb-3">
                                                            <input type="text" class="form-control" id="OfficePHoneNumber" placeholder="Office PHone Number">
                                                            <label for="OfficePHoneNumber">Office PHone Number <span class="text-danger">*</span> </label>
                                                        </div>
                                                    </div>
                                                    <!-- / col --> 
                                                    <!-- col -->
                                                    <div class="col-md-6 col-lg-3">
                                                        <div class="form-floating mb-3">
                                                            <input type="text" class="form-control" id="LandMark" placeholder="Land Mark">
                                                            <label for="LandMark">Land Mark <span class="text-danger">*</span></label>
                                                        </div>
                                                    </div>
                                                    <!-- / col --> 

                                                     <!-- col -->
                                                    <div class="col-md-6 col-lg-3">
                                                        <div class="form-floating mb-3">
                                                            <input type="text" class="form-control" id="ExistingEMIAmount" placeholder="Existing EMI Amount">
                                                            <label for="ExistingEMIAmount">Existing EMI Amount</label>
                                                        </div>
                                                    </div>
                                                    <!-- / col --> 
                                               
                                            </div>
                                            <!-- row -->
                                          <!-- Professional details -->
                                     </div>  

                                      <div class="tab-pane fade" id="documents" role="tabpanel" aria-labelledby="documents-tab">
                                          <!-- Documents -->
                                           <!-- row -->
                                            <div class="row">
                                                <!-- col -->
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="PANCARD" class="form-label">PAN CARD <span class="text-danger">*</span></label>
                                                        <input class="form-control" type="file" id="PANCARD">
                                                    </div>
                                                </div>
                                                <!-- / col -->

                                                 <!-- col -->
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="Address" class="form-label">Address Proof (DL / Passport / Aadhar /  Voter) <span class="text-danger">*</span></label>
                                                        <input class="form-control" type="file" id="Address">
                                                    </div>
                                                </div>
                                                <!-- / col -->

                                                   <!-- col -->
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="Passport" class="form-label">Recent Passport Size Photo <span class="text-danger">*</span> </label>
                                                        <input class="form-control" type="file" id="Passport">
                                                    </div>
                                                </div>
                                                <!-- / col -->

                                                <!-- col -->
                                                <div class="col-lg-6">
                                                    <div class="mb-3">
                                                        <label for="CustomerSignature" class="form-label">Customer Signature <span class="text-danger">*</span></label>
                                                        <input class="form-control" type="file" id="CustomerSignature">
                                                    </div>
                                                </div>
                                                <!-- / col -->
                                                 </div>
                                                <!--/ row -->

                                                <!-- card -->
                                                <div class="card mb-3">
                                                    <!-- header -->
                                                    <div class="card-header">
                                                        <h5 class="fbold h5">Last 2 Months Payslips with password</h5>
                                                    </div>
                                                    <!--/ header -->
                                                    <!-- card body -->
                                                    <div class="card-body">
                                                        <!-- row -->
                                                        <div class="row">                                                   
                                                            <!-- col -->
                                                            <div class="col-lg-6">
                                                                <div class="mb-3 row">
                                                                    <div class="col-lg-8">
                                                                        <label for="LatestPayslip" class="form-label">Latest Payslip 1 <span class="text-danger">*</span> </label>
                                                                        <input class="form-control" type="file" id="LatestPayslip">
                                                                    </div>
                                                                    <div class="col-lg-4">
                                                                        <label for="password1" class="form-label">Create Password <span class="text-danger">*</span> </label>
                                                                        <input class="form-control" type="password" id="password1">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- / col -->
                                                            <!-- col -->
                                                            <div class="col-lg-6">
                                                                <div class="mb-3 row">
                                                                    <div class="col-lg-8">
                                                                        <label for="LatestPayslip2" class="form-label">Latest Payslip 2 <span class="text-danger">*</span> </label>
                                                                        <input class="form-control" type="file" id="LatestPayslip2">
                                                                    </div>
                                                                    <div class="col-lg-4">
                                                                        <label for="password1" class="form-label">Create Password <span class="text-danger">*</span> </label>
                                                                        <input class="form-control" type="password" id="password1">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- / col -->                                                   
                                                        </div>
                                                        <!--/ row -->
                                                    </div>
                                                    <!--/ card body -->
                                                </div>
                                                <!-- card -->
                                               
                                                

                                            <!-- card -->
                                            <div class="card">
                                                <div class="card-header">
                                                    <h5 class="fbold h5">Last 3 Months Individual Bank Statements with password</h5>
                                                </div>
                                                <!-- card body -->
                                                <div class="card-body">
                                                    <!-- row -->
                                                    <div class="row">                                                   
                                                        <!-- col -->
                                                        <div class="col-lg-6">
                                                            <div class="mb-3 row">
                                                                <div class="col-lg-8">
                                                                    <label for="monthone" class="form-label">Month 1 <span class="text-danger">*</span> </label>
                                                                    <input class="form-control" type="file" id="monthone">
                                                                </div>
                                                                <div class="col-lg-4">
                                                                    <label for="password1" class="form-label">Create Password <span class="text-danger">*</span> </label>
                                                                    <input class="form-control" type="password" id="password1">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- / col -->

                                                        <!-- col -->
                                                        <div class="col-lg-6">
                                                            <div class="mb-3 row">
                                                                <div class="col-lg-8">
                                                                    <label for="monthtwo" class="form-label">Month 2 <span class="text-danger">*</span> </label>
                                                                    <input class="form-control" type="file" id="monthtwo">
                                                                </div>
                                                                <div class="col-lg-4">
                                                                    <label for="password1" class="form-label">Create Password <span class="text-danger">*</span> </label>
                                                                    <input class="form-control" type="password" id="password1">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- / col -->        
                                                        
                                                        <!-- col -->
                                                        <div class="col-lg-6">
                                                            <div class="mb-3 row">
                                                                <div class="col-lg-8">
                                                                    <label for="monththree" class="form-label">Month 3 <span class="text-danger">*</span> </label>
                                                                    <input class="form-control" type="file" id="monththree">
                                                                </div>
                                                                <div class="col-lg-4">
                                                                    <label for="password1" class="form-label">Create Password <span class="text-danger">*</span> </label>
                                                                    <input class="form-control" type="password" id="password1">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- / col -->  
                                                    </div>
                                                    <!--/ row -->
                                                    </div>
                                                    <!--/ card body -->
                                            </div>
                                            <!-- card -->                                       
                                        </div>  
                                        <!-- Documents -->

                                      <div class="tab-pane fade" id="bankInfo" role="tabpanel" aria-labelledby="bankInfo-tab">
                                          <!-- Bank Info -->
                                           <!-- row -->
                                            <div class="row">
                                                <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <input type="text" class="form-control" id="BankName" placeholder="Bank Name">
                                                        <label for="BankName">Bank Name <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <!-- / col --> 
                                                <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <input type="text" class="form-control" id="IFSCCode" placeholder="IFSC Code">
                                                        <label for="IFSCCode">IFSC Code <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <!-- / col --> 
                                                <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <input type="text" class="form-control" id="Branch" placeholder="Enter Branch Name" value="Kukatpally">
                                                        <label for="Branch">Branch </label>
                                                    </div>
                                                </div>
                                                <!-- / col --> 
                                                <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <input type="text" class="form-control" id="NameAsperAccount" placeholder="Name As per Account">
                                                        <label for="NameAsperAccount">Name As per Account <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <!-- / col --> 
                                                <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <input type="text" class="form-control" id="AccountNumber" placeholder="Account Number">
                                                        <label for="AccountNumber">Account Number <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <!-- / col --> 
                                                 <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <input type="text" class="form-control" id="ReEnterAccountNumber" placeholder="Re Enter Account Number">
                                                        <label for="ReEnterAccountNumber">Re Enter Account Number <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <!-- / col -->
                                               
                                            </div>
                                            <!-- row -->
                                          <!-- Bank Info -->
                                     </div>  

                                     <div class="tab-pane fade" id="references" role="tabpanel" aria-labelledby="references-tab">
                                          <!-- Reference Info -->
                                           <!-- row -->
                                            <div class="row">
                                                 <!-- col -->
                                                <div class="col-md-12">
                                                    <h5 class="h5 fbold">Reference 01</h5>
                                                </div>
                                                <!--/ col -->
                                               <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <input type="text" class="form-control" id="ReferenceName" placeholder="Reference Name">
                                                        <label for="ReferenceName">Reference Name <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <!-- / col -->
                                                <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <select class="form-select"  id="Gender" aria-label="Gender">
                                                            <option>Male</option>
                                                            <option>Female</option>
                                                        </select>
                                                        <label for="Gender">Gender <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <!-- / col -->   
                                                 <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <input type="text" class="form-control" id="ReferenceMobileNumber" placeholder="Reference Mobile Number">
                                                        <label for="ReferenceMobileNumber">Reference Mobile No <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <!-- / col -->
                                                <!-- col -->
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-floating mb-3">
                                                        <select class="form-select"  id="Relation" aria-label="Relation">
                                                            <option>Friend</option>
                                                            <option>Brother</option>
                                                        </select>
                                                        <label for="Relation">Relation <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <!-- / col -->   
                                                <!-- col -->
                                                <div class="col-md-12">
                                                    <div class="form-floating mb-3">
                                                        <textarea class="form-control" placeholder="Remarks" id="Remarks"></textarea>
                                                        <label for="Remarks">Remarks</label>
                                                    </div>
                                                </div>
                                                <!-- / col -->                                              

                                                <!-- col -->
                                                <div class="col-md-12 text-end">
                                                    <button class="btnCustom" disabled>Submit</button>
                                                </div>
                                                <!--/ col -->
                                               
                                            </div>
                                            <!-- row -->
                                          <!-- Reference Info -->
                                     </div>  
                                     
                                     
                                     
                                     
                                     </div>
                                </div>
                            </div>
                            <!--/ tab -->
                        </div>
                       
                    </div>    
                </div>
            </div>
            <!--/ sub page Body Content-->
        </section>
        <!--/ subpage body -->
    </main>
    <!--/ main --> 
    <?php include 'includes/footer.php' ?> 
    <?php include 'includes/scripts.php'?>
</body>

</html>