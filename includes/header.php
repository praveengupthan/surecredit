  <div id="load"></div>
  <!-- header -->
    <header class="fixed-top">
        <div class="containerCustom">
            <div class="navbar navbar-expand-xl bsnav"><a class="navbar-brand" href="index.php">
                    <img src="img/logo.svg" alt="">
                </a>
                <button class="navbar-toggler toggler-spring"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse">
                    <ul class="navbar-nav navbar-mobile mr-0">
                        <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='index.php'){echo'active';}else {echo'nav-link';}?>" href="index.php">Home</a></li>
                        <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='personalLoans.php'){echo'active';}else {echo'nav-link';}?>" href="personalLoans.php">Personal Loans</a></li>
                        <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='howitworks.php'){echo'active';}else {echo'nav-link';}?>" href="howitworks.php">How it Works</a></li>
                        <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='about.php'){echo'active';}else {echo'nav-link';}?>" href="about.php">About us</a></li>
                        <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='faq.php'){echo'active';}else {echo'nav-link';}?>" href="faq.php">Faq's</a></li>
                        <li class="nav-item dropdown pop"><a class="nav-link" href="javascript:void(0)">Has children! <i class="caret"></i></a>
                            <ul class="navbar-nav">
                                <li class="nav-item"><a class="nav-link" href="#">Action</a></li>                              
                                <li class="nav-item"><a class="nav-link" href="#">Other action</a></li>
                            </ul>                    
                    </li>                        
                        <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='contact.php'){echo'active';}else {echo'nav-link';}?>" href="contact.php">Contact</a></li>
                    </ul>
                    <ul class="navbar-nav navbar-mobile ms-auto">
                        <li class="nav-item"><a class="nav-link applybtn" href="userApplyLoan.php">Apply Loan</a></li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" 
                                aria-controls="offcanvasRight">My Profile <span class="icon-navbar icomoon"></span></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="bsnav-mobile">
                <div class="bsnav-mobile-overlay"></div>
                <div class="navbar"></div>
            </div>
        </div>
    </header>
    <!--/ header -->
    <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasRight" aria-labelledby="offcanvasRightLabel">
        <div class="offcanvas-header">
            <h5 id="offcanvasRightLabel">Welcome Guest!</h5>
            <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <div class="offcanvas-body">
            <div class="profileNav">
                <div class="userProfilePic text-center">
                    <span class="icon-user icomoon"></span>
                </div>
                <ul class="userNav">
                    <li><a href="login.php">Login</a></li>
                    <li><a href="register.php">Register</a></li>                    
                </ul>
                <div class="contactHeader position-absolute">
                    <p class="border-bottom pb-2"><span class="icon-telephone icomoon"></span> <span>+91 9642123254</span>
                    </p>
                    <p class="border-bottom pb-2"><span class="icon-email01 icomoon"></span>
                        <span>info@surecredit.com</span>
                    </p>
                    <p class="border-bottom pb-2"><span class="icon-location icomoon"></span> <span>H.No:325,
                            Pragathi Nagar colony, Plot No;320, Road No;4, Hyderabad - 500072, Telangana</span></p>
                    <div class="footerSocial pb-3">
                        <a href="javascript:void(0)" class="d-inline-block text-center me-2"><span
                                class="icon-facebook icomoon"></span></a>
                        <a href="javascript:void(0)" class="d-inline-block text-center me-2"><span
                                class="icon-twitter2 icomoon"></span></a>
                        <a href="javascript:void(0)" class="d-inline-block text-center me-2"><span
                                class="icon-linkedin2 icomoon"></span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>