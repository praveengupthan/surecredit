<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sure Credit</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/fav.png"> 
    <?php include 'includes/styles.php'?>
    <?php include 'includes/data.php'?>
</head>

<body> <?php include'includes/header.php' ?>
    <!-- main -->
    <main class="subpageMain">
        <!-- subpage header -->
        <section class="subpageHeader">
            <!-- container -->
            <div class="container">
                <article>
                    <h6 class="h6">Overview</h6>
                    <h1>About Sure Credit</h1>
                </article>
            </div>
            <!--/ container -->
        </section>
        <!--/ sub page header -->

        <!-- subpage body -->
        <section class="subpageBody">
            <!-- container -->
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>                        
                        <li class="breadcrumb-item active" aria-current="page">About us</li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
            <!-- sub page Body Content -->
            <div class="subpageBodyContent">
                <!-- container  -->
                <div class="container">
                     <div class="row justify-content-center">
                         <div class="col-md-10 text-center">
                            <h2 class="h2 fbold">Who we are?</h2>
                            <p>SureCredit is an Instant Personal Loan fintech company that provides fast, flexible, unsecured loans to salaried professionals across India. The online platform is backed by a strong team. We aim to become the first choice when it comes to quick and convenient Personal Loans. The application process is also completely online with minimal documentation because we understand that when you’re in urgent need of funds, long waiting times can be very frustrating, so we eliminated that. We offer loans in varying ticket sizes and repayment tenures, to suit all your unpredictable financial needs. We differentiate in otherwise cluttered Personal Loan segment and deliver fastest Personal Loans at customer friendly terms.</p>
                         </div>
                     </div>
                 </div>
                 <!--/ container -->
                  <!-- container  -->
                 <div class="container">
                     <div class="row py-2 py-lg-5">
                        <div class="col-md-6">
                            <img src="img/mission.svg" alt="" class="img-fluid">
                        </div>
                        <div class="col-md-6 align-self-center">
                            <h2 class="fbold fblue">Our Mission and Vision</h2>
                            <p>Your well-being is our primary focus. We’re honest and transparent about everything we do, whether it’s you, our employees, or even our partners. </p>
                            <p>To promote financial inclusion by creating simple, transparent products that empower individuals to live a life of choice and freedom.</p>
                            <p>Our products are designed to help you grow and achieve more. To this end, we want you to achieve your goals in life and be control of your money. Our Leadership. We are situated in Hyderabad, South India particularizing in offering instant monetary options. </p>
                        </div>
                     </div>

                    <div class="row py-2 py-lg-5">
                        <div class="col-md-6 order-lg-last">
                            <img src="img/vision.svg" alt="" class="img-fluid">
                        </div>
                        <div class="col-md-6 align-self-center">
                            <h2 class="fbold fblue">What We Value?</h2>
                            <p>The reason why we are one of the best instant loan apps in India is two-fold. Our customers have always found our interface to be user-friendly and enticing. The registration process is simple and comprises only a handful of steps. We standby people during unyielding conditions of life, where they are strapped for cash and unable to come out of it. </p>
                            <h5 class="h5 fbold">Values</h5>
                            <ul class="listItems">
                                <li><span class="fsbold fgreen">Respect </span>A culture to respect people you deal with</li>
                                <li><span class="fsbold fgreen">Integrity </span>To be ethical and sincere in all actions</li>
                                <li><span class="fsbold fgreen">Committed </span>customer-focused & Dedicated</li>
                                <li><span class="fsbold fgreen">Innovative </span>Always strive, never settle</li>
                                <li>Do the right thing</li>
                            </ul>
                        </div>
                    </div>                   
                </div>
                <!--/ container -->

                <div class="ourPromise py-2 py-lg-5">
                      <div class="container">
                           <!-- row -->
                            <div class="row justify-content-center">
                                <div class="col-md-6 text-center">
                                    <figure>
                                        <img src="img/leader.jpg" class="leaderImg" alt="">
                                    </figure>
                                    <h2 class="h2 fbold pt-3">Our Promise</h2>
                                    <p>SureCredit India promises to stay with you in every step by offering you a truly modern solution for an instant personal loan. With the paperless fully digitalized process, with instant processing time, attractive interest rate, and without repayment charges. </p>
                                    <p class="fgreen fsbold"><i>Vijay Sharma, <small>Chairman & Founder</small></i></p>
                                </div>
                            </div>
                            <!--/ row -->
                      </div>
                </div>

             
            </div>
            <!--/ sub page Body Content-->
        </section>
        <!--/ subpage body -->
    </main>
    <!--/ main --> 
    <?php include 'includes/footer.php' ?> 
    <?php include 'includes/scripts.php'?>
</body>

</html>