<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sure Credit Privacy</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/fav.png"> 
    <?php include 'includes/styles.php'?>
    <?php include 'includes/data.php'?>
</head>

<body> <?php include'includes/header.php' ?>
    <!-- main -->
    <main class="subpageMain">
        <!-- subpage body -->
        <section class="subpageBody">           
            <!-- sub page Body Content -->
            <div class="applypage">
                <div class="container">
                    <div class="row py-3 py-lg-5">
                         <div class="col-lg-6 d-none d-lg-block">
                            <img src="img/loginimg.svg" class="img-fluid" alt="">
                        </div>
                        <div class="col-lg-6 align-self-center">
                            <div class="signCol">
                                <article class="text-center">
                                    <h2 class="h2 fbold">Enter Your OTP</h2>     
                                    <p>+91 9642123254 <a href="login.php"><b>Edit</b></a></p>                               
                                </article>
                                <form class="applyLoanform">
                                    <div class="form-floating mb-3">
                                        <input type="text" class="form-control" id="phoneEnter" placeholder="Enter Phone Number To verify">
                                        <label for="phoneEnter">Enter OTP Here</label>
                                    </div>
                                    <p><a href="javascript:void(0)">Resent</a> OTP in 95s</p>
                                                                  
                                </form>                                
                                <button class="w-100" onClick="window.location.href='userDashboard.php';">Verify</button>                             

                            </div>
                        </div>
                       
                    </div>    
                </div>
            </div>
            <!--/ sub page Body Content-->
        </section>
        <!--/ subpage body -->
    </main>
    <!--/ main --> 
    <?php include 'includes/footer.php' ?> 
    <?php include 'includes/scripts.php'?>
</body>

</html>