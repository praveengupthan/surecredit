 <!-- home slider -->
        <section class="homeSlider">
            <!-- MasterSlider -->
            <div id="P_masterslider" class="master-slider-parent ms-parent-id-15">
                <!-- MasterSlider Main -->
                <div id="masterslider" class="master-slider ms-skin-minimal">
                    <div class="ms-slide" data-delay="3" data-fill-mode="fill">
                        <img src="images/blank.gif" alt="" title="bg-slide-1" data-src="img/slider01.jpg" />
                        <div class="ms-layer msp-cn-160-1" style="width:500px;"
                            data-effect="t(true,n,50,n,n,n,n,n,n,n,n,n,n,n,n)" data-duration="1600"
                            data-ease="easeOutQuart" data-offset-x="119" data-offset-y="-50" data-origin="ml"
                            data-position="normal" data-masked="true">
                            <h6>Simple Credit Solution</h6>
                        </div>
                        <div class="ms-layer msp-cn-160-66" style="" data-effect="t(true,200,n,n,n,n,n,n,n,n,n,n,n,n,n)"
                            data-duration="2400" data-delay="400" data-ease="easeOutQuint" data-offset-x="119"
                            data-offset-y="30" data-origin="ml" data-position="normal" data-masked="true">One Stop for All Money  <br>  Solutions</div>
                        <a href="about.php" target="_self"
                            class="ms-layer msp-cn-160-5 ms-btn ms-btn-box ms-btn-n msp-preset-btn-189 d-none"
                            data-effect="t(true,n,n,-250,n,n,n,n,n,n,n,n,n,n,n)" data-duration="1500" data-delay="1000"
                            data-ease="easeOutQuint" data-type="button" data-action="next" data-offset-x="120"
                            data-offset-y="144" data-origin="ml" data-position="normal">More Info</a>
                    </div>
                    <div class="ms-slide" data-delay="4" data-fill-mode="fill">
                        <img src="images/blank.gif" alt="" title="bg-slide-1" data-src="img/slider02.jpg" />
                        <div class="ms-layer msp-cn-160-1" style="width:500px;"
                            data-effect="t(true,n,50,n,n,n,n,n,n,n,n,n,n,n,n)" data-duration="1600"
                            data-ease="easeOutQuart" data-offset-x="119" data-offset-y="-50" data-origin="ml"
                            data-position="normal" data-masked="true">
                            <h6>SureCredit Instant Cash Offer</h6>
                        </div>
                        <div class="ms-layer msp-cn-160-3" style="" data-effect="t(true,200,n,n,n,n,n,n,n,n,n,n,n,n,n)"
                            data-duration="2400" data-ease="easeOutQuart" data-offset-x="119" data-offset-y="30"
                            data-origin="ml" data-position="normal" data-masked="true">Instant Cash For Assessing your cash needs <br>
                            Making Finance Simple.</div>
                        <a href="about.php" target="_self"
                            class="ms-layer msp-cn-160-5 ms-btn ms-btn-box ms-btn-n msp-preset-btn-189 d-none"
                            data-effect="t(true,n,n,-250,n,n,n,n,n,n,n,n,n,n,n)" data-duration="1500" data-delay="1000"
                            data-ease="easeOutQuint" data-type="button" data-action="next" data-offset-x="120"
                            data-offset-y="144" data-origin="ml" data-position="normal">More Info</a>
                    </div>
                    <div class="ms-slide" data-delay="4" data-fill-mode="fill">
                        <img src="images/blank.gif" alt="" title="bg-slide-1" data-src="img/slider03.jpg" />
                        <div class="ms-layer msp-cn-160-1" style="width:500px;"
                            data-effect="t(true,n,50,n,n,n,n,n,n,n,n,n,n,n,n)" data-duration="1600"
                            data-ease="easeOutQuart" data-offset-x="119" data-offset-y="-50" data-origin="ml"
                            data-position="normal" data-masked="true">
                            <h6>Trust SureCredit for fast and simple Loans</h6>
                        </div>
                        <div class="ms-layer msp-cn-160-66" style="" data-effect="t(true,200,n,n,n,n,n,n,n,n,n,n,n,n,n)"
                            data-duration="2400" data-delay="400" data-ease="easeOutQuint" data-offset-x="119"
                            data-offset-y="30" data-origin="ml" data-position="normal" data-masked="true">We make everyday financial operations  <br> simple and effective </div>
                        <a href="about.php" target="_self"
                            class="ms-layer msp-cn-160-5 ms-btn ms-btn-box ms-btn-n msp-preset-btn-189 d-none"
                            data-effect="t(true,n,n,-250,n,n,n,n,n,n,n,n,n,n,n)" data-duration="1500" data-delay="1000"
                            data-ease="easeOutQuint" data-type="button" data-action="next" data-offset-x="120"
                            data-offset-y="144" data-origin="ml" data-position="normal">More Info</a>
                    </div>
                </div>
                <!-- END MasterSlider Main -->
            </div>
            <!-- END MasterSlider -->
        </section>
        <!--/ home slider -->