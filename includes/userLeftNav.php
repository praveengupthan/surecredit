 <!-- left nav bar -->
<div class="col-lg-4">
    <div class="shadowBox leftNav">                      
        <!-- user details -->
        <div class="userBase p-2 p-lg-4">
            <div class="d-flex">
                <figure>
                    <img class="userPic" alt="" src="img/profile_user.jpg">
                </figure>
                <article>
                    <h5 class="h5 text-uppercase fbold">Praveen Kumar</h5>
                    <p class="m-0 p-0 opacitytext">praveenn@gmail.com</p>
                    <p class="m-0 p-0 opacitytext ">+91 9642123254</p>
                </article> 
            </div>
            <p class="d-none d-lg-block">I constantly challenge myself to solve problems by design.</p>
            <a href="index.php" class="d-block w-100">Logout</a> 
        </div>
        <!--/ user details -->
        <!-- nav -->
        <ul class="nav navbar-nav">
            <li class="nav-item">
                <a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='userDashboard.php'){echo'active';}else {echo'nav-link';}?>" href="userDashboard.php"><span class="icon-dashboard2 icomoon"></span> My Dashboard</a>
            </li>
            <li class="nav-item">
                <a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='userLoans.php'){echo'active';}else {echo'nav-link';}?>" href="userLoans.php"><span class="icon-loan3 icomoon"></span> My Loans</a>
            </li>
            <li class="nav-item">
                <a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='userkycDocuments.php'){echo'active';}else {echo'nav-link';}?>" href="userkycDocuments.php"><span class="icon-verification icomoon"></span> Documents</a>
            </li>    
            <li class="nav-item">
                <a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='userApplyLoan.php'){echo'active';}else {echo'nav-link';}?>" href="userApplyLoan.php"><span class="icon-register icomoon"></span> Apply Loan</a>
            </li>         
            <li class="nav-item">
                <a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='userRepayLoan.php'){echo'active';}else {echo'nav-link';}?>" href="userRepayLoan.php"><span class="icon-money icomoon"></span>Repay Loan</a>
            </li>                           
        </ul>
        <!--/ nav -->
    </div>
</div>
<!--/ left nav bar -->