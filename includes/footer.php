 <!-- footer -->
    <footer>
        <!-- top section -->
        <div class="topFooter">
            <a href="javascript:void(0)" class="moveTop" id="moveTop"><span class="icon-uparrow icomoon"></span></a>
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- left col -->
                    <div class="col-md-4 leftFooterCol">
                        <a class="footerBrand my-3 d-inline-block" href="javascript:void(0)">
                            <img src="img/logo.svg" alt="" class="img-fluid">
                        </a>
                        <p class="border-bottom pb-3 footerabout">Our menu is a nod to street food vendors who help their customers
                            stay on-the-go by delivering quick tasty bites. We incorporated flavors from around the
                            world to offer a unique menu <a href="about.php" class="fsbold">Read More...</a>
                        </p>
                        <p><span class="icon-email01 icomoon"></span> info@surecredit.in </p>
                        <p><span class="icon-telephone2 icomoon"></span> +91 799518956</p>
                        <div class="footerSocial pb-3">
                            <a href="javascript:void(0)" class="d-inline-block text-center me-1"><span
                                    class="icon-facebook icomoon"></span></a>
                            <a href="javascript:void(0)" class="d-inline-block text-center me-1"><span
                                    class="icon-twitter2 icomoon"></span></a>
                            <a href="javascript:void(0)" class="d-inline-block text-center"><span
                                    class="icon-linkedin2 icomoon"></span></a>
                        </div>
                    </div>
                    <!--/ let col -->
                    <!-- right col -->
                    <div class="col-md-8 ">
                        <div class="rtFootrecol p-3 p-lg-5">
                            <div class="row">
                                <div class="col-md-3 col-sm-6">
                                    <h5 class="h5 fbold">Company</h5>
                                    <ul class="pt-3 footerLink">
                                        <li><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='index.php'){echo'active';}else {echo'nav-link';}?>" href="index.php">Home</a></li> 
                                        <li><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='about.php'){echo'active';}else {echo'nav-link';}?>" href="about.php">About us</a></li>                                        
                                        <li><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='contact.php'){echo'active';}else {echo'nav-link';}?>" href="contact.php">Contact</a></li>                                        
                                        <li><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='applyloan.php'){echo'active';}else {echo'nav-link';}?>" href="applyloan.php">Apply Loan</a></li>
                                        <li><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='personalLoans.php'){echo'active';}else {echo'nav-link';}?>" href="personalLoans.php">Personal Loan</a></li>
                                    </ul>
                                </div>                               
                                <div class="col-md-3 col-sm-6">
                                    <h5 class="h5 fbold">Support</h5>
                                    <ul class="pt-3 footerLink">                                        
                                        
                                        <li><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='faq.php'){echo'active';}else {echo'nav-link';}?>" href="faq.php">Faq</a></li>
                                        <li><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='terms.php'){echo'active';}else {echo'nav-link';}?>" href="terms.php">Terms & Conditions</a></li>
                                        <li><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='privacy.php'){echo'active';}else {echo'nav-link';}?>" href="privacy.php">Privacy Policy</a></li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <h5 class="h5 fbold">Get the app now</h5>
                                    <p>If you are looking for instant cash loan to meet your urgent needs, We are here! Get Loan without physical verification and without document in 1 hour with our AI-based system</p>
                                     <a href="javascript:void(0)" target="_blank">
                                        <img src="img/googleplayimg.png" alt="" class="appimgand">
                                    </a>                                            
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/ right col -->
                </div>
                <!--/ row -->
            </div>
        </div>
        <!--/ top section -->
        <!-- bottom footer -->
        <div class="bottomFooter text-center">
            <p class="m-0"><small>©Copyright 2021-SureCredit.com -All Rights Reserved</small></p>
        </div>
        <!--/ bottom footer -->
    </footer>
    <!--/ footer -->