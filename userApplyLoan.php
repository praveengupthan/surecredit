<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sure Credit</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/fav.png"> 
    <?php include 'includes/styles.php'?>
    <?php include 'includes/data.php'?>
</head>

<body> <?php include'includes/headerPostLogin.php' ?>
    <!-- main -->
    <main class="subpageMain profileSubPage">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <?php include 'includes/userLeftNav.php'?>
                <!-- right section -->
                <div class="col-lg-8">
                    <div class="shadowBox">
                        <!-- content -->
                        <div class="profileContent p-2 p-lg-4">
                            <!-- row -->
                            <div class="row mb-4">
                                <div class="col-md-12">
                                    <h5 class="flight h5 border-bottom mb-3 pb-3"><span class="fbold fblue">Apply Loan</span></h5>
                                </div>                              
                            </div>
                            <!--/ row -->
                            <!-- apply loan content -->
                              <p>I Wish to pay</p>
                                <div class="row">
                                    <!-- col -->
                                    <div class="col-lg-6">
                                        <div class="form-check form-check-inline border p-3 d-block">
                                            <input class="form-check-input ms-2 me-3" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                            <label class="form-check-label" for="inlineRadio1">EMI for 95 Days (3 Months & 5 days) </label>
                                        </div>
                                    </div>
                                    <!--/ col -->
                                    <!-- col -->
                                    <div class="col-lg-6">
                                        <div class="form-check form-check-inline border p-3 d-block">
                                            <input class="form-check-input ms-2 me-3" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                            <label class="form-check-label" for="inlineRadio2">EMI for 65 Days (2 Months & 5 days) </label>
                                        </div>
                                    </div>
                                    <!--/ col -->
                                    <!-- col -->
                                    <div class="col-lg-6">
                                        <div class="form-check form-check-inline border p-3 d-block">
                                            <div class="d-flex">
                                                <input class="form-check-input ms-2 me-3" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3">
                                                <label class="form-check-label me-3" for="inlineRadio3">Enter the Days </label>
                                                <input type="text" class="w-50 m-0 align-self-center">
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ col -->
                                </div>

                                <!-- ranger -->
                                <div class="ranger">
                                    <p>Drag the slider to display the current value.</p>
                                    <div class="slidecontainer py-3">
                                        <input type="range" min="10000" max="2500000" value="50" class="slider" id="myRange" step="10000">
                                        <p class="d-flex justify-content-between pt-3"><span>Loan Amount:</span> <span>Rs: <span id="demo" class="fbold"></span>.00</span></p>
                                    </div>
                                </div>
                                <!--/ ranger -->

                                <div class="priceItem py-2 mb-2 border-bottom">
                                    <p class="d-flex justify-content-between">
                                        <span>Total Loan Amount</span>
                                        <span>Rs: 10,000.00</span>
                                    </p>
                                </div>

                                <div class="priceItem py-2 mb-2 border-bottom">
                                    <p class="d-flex justify-content-between">
                                        <span>Payble  Amount</span>
                                        <span>Rs: 10,250.00</span>
                                    </p>
                                </div>

                                <div class="priceItem py-2 mb-2 border-bottom">
                                    <p class="d-flex justify-content-between">
                                        <span>You will pay Extra Amount</span>
                                        <span>Rs: 2,250.00</span>
                                    </p>
                                </div>

                                <div class="priceItem py-2 mb-2 border-bottom">
                                    <p class="d-flex justify-content-between">
                                        <span>Bank Interest Rate</span>
                                        <span>8.5%</span>
                                    </p>
                                </div>

                                <div class="priceItem py-2 mb-2 border-bottom">
                                    <p class="d-flex justify-content-between">
                                        <span>Processing Fee</span>
                                        <span>1.5%</span>
                                    </p>
                                </div>

                                <div class="row justify-content-center py-4">
                                    <div class="col-md-8 text-center">
                                            <div class="d-flex justify-content-center">
                                                <div>
                                                    <h2 class="h2 fsbold mb-0 pb-0">EMI</h2>
                                                    <small>Per month</small>
                                                </div>
                                                <div class="ps-4">
                                                    <h1 class="h1 fbold">Rs: 2,882</h1>                                        
                                                </div>
                                            </div>
                                    </div>
                                </div>

                                
                                <div class="acceptterms pb-3">
                                    <span>I am Agree with</span>
                                    <div class="form-check form-check-inline ps-0 ">
                                        <input class="form-check-input ms-1" type="checkbox" value="option1">
                                        <label class="form-check-label ms-1"> <a href="terms.php" target="_blank">Terms &amp; Conditions</a> </label>
                                    </div>
                                    <div class="form-check form-check-inline">                                       
                                        <input class="form-check-input" type="checkbox" value="option2">
                                        <label class="form-check-label"><a href="privacy.php" target="_blank">Privacy Policy</a></label>
                                    </div>
                                </div>

                                  <!-- table -->
                            <div class="table-responsive customTable">
                                <table class="table table-striped table-hover repayloan">
                                    <thead>
                                        <tr>
                                        <th scope="col">Select Tenure</th>
                                        <th scope="col">Monthly EMI Amount</th>                                       
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>                                          
                                            <td><input type="radio"><span class="d-inline-block ps-2"  name="inlineRadioOptions" id="inlineRadio3" value="option1">2 Months</span></td>
                                            <td><label for="inlineRadio3">Rs: 5455.00</label></td>                                           
                                        </tr>  
                                         <tr>                                          
                                            <td><input type="radio"><span class="d-inline-block ps-2"  name="inlineRadioOptions" id="inlineRadio4" value="option2">4 Months</span></td>
                                            <td><label for="inlineRadio4">Rs: 2455.00</label></td>                                           
                                        </tr>                                      
                                    </tbody>
                                </table>
                            </div>
                            <!--/ table -->

                                <button class="btnCustom">Apply Now</button>

                            <!--/ apply loan content -->
                        </div>
                        <!--/ content -->
                    </div>
                </div>
                <!--/ right section -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->

    </main>
    <!--/ main --> 
    <?php include 'includes/footer.php' ?> 
    <?php include 'includes/scripts.php'?>

     <script>
        var slider = document.getElementById("myRange");
        var output = document.getElementById("demo");
        output.innerHTML = slider.value;

        slider.oninput = function() {
        output.innerHTML = this.value;
        }
    </script>
</body>

</html>