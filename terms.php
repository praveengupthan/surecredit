<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sure Credit Terms &amp; Conditions</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/fav.png"> 
    <?php include 'includes/styles.php'?>
    <?php include 'includes/data.php'?>
</head>

<body> <?php include'includes/header.php' ?>
    <!-- main -->
    <main class="subpageMain">
        <!-- subpage header -->
        <section class="subpageHeader">
            <!-- container -->
            <div class="container">
                <article>
                    <h6 class="h6">Privacy</h6>
                    <h1>Terms &amp; Conditions</h1>
                </article>
            </div>
            <!--/ container -->
        </section>
        <!--/ sub page header -->

        <!-- subpage body -->
        <section class="subpageBody">
            <!-- container -->
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>                        
                        <li class="breadcrumb-item active" aria-current="page">Terms &amp; Conditions</li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
            <!-- sub page Body Content -->
            <div class="subpageBodyContent">
               <!-- container -->
               <div class="container">
                    <h4 class="h4 fsbold">INTRODUCTION</h4>
                    <p>The website “www.surecredit.in” (“theWebsite” or “site”) and mobile application “Surecredit.in” (“App”) (collectively referred to as “Platform”) is owned and operated by Finxer Technologies Private Limited (“the Company” or “Surecredit” or “we” or “us” or “our”) with registered address at Office Space No. 203, Second Floor, KKR Square, Kavuri Hills Phase1, Jubilee Hills, Hyderabad, Telangana- 500033. </p>
                  
                    <p>Your use of this Platform implies that You have read, understood and agreed to abide by following, disclaimer, terms & conditions ("Terms & Conditions"). “Customer(s)" or "you" or “User” or "End-Users" shall mean any person who accesses, downloads, uses or views the Platform and the Services (as defined below).  </p>

                    <p>SureCredit is a platform facilitating the loan transactions between the borrowers. All loan applications are approved and sanctioned by loans from various non-banking financial companies or banks (“Lender” or “Lending Partner”). The objective of the Platform is to facilitate the lending and borrowing of capital, in accordance with the terms of the loan agreement and other documentation to be executed between the Lender and borrower/you whether in physical or electronic form, without any recourse to Surecredit. Surecredit operates the Platform as an intermediary for the purpose of connecting you with the Lending Partners and not in the capacity of a non-banking financial company-peer to peer lending platform (NBFC-P2P), as defined under the Master Directions-Non-Banking Financial Company- Peer to Peer Lending Platform (Reserve Bank) Directions, 2017. Before using our Services, you are requested to please ensure that you carefully read and understand these User T & C which shall apply. Your use of our Services is expressly conditional on your acceptance of these User T & C.</p>

                    <div class="ps-md-5">
                        <p>"Customer(s)" or "you" or "End-Users" shall mean any person who accesses, downloads, uses, views the Platform and the Services.</p>
                        <p>"Loan" shall mean the loan that you may apply for through the Platform and which is sanctioned and granted by Lender, subject to the applicable terms and conditions of the loan agreement.</p>
                        <p>"Loan Agreement" shall mean the loan agreement to be executed between Lender and the borrower/you for granting the Loan.</p>
                        <p>"Online Stores" shall mean Windows Store, Android Google Play, iOS App store or any other online store or portal where the App will be made available by the Company to the End-Users, from time to time.</p>
                        <p>"Outstanding Amount(s)" shall mean the principal amount of Loan, interest and applicable charges in accordance with the terms of the Loan Agreement due and payable by borrower/you to Lender, on respective due date(s).</p>
                        <p>“Personal Information” means information provided by you to the Company that can be used directly or indirectly to identify you, including through your mobile number or email address, either from that information alone or from that information combined with other information the Platform has/may have access to, about you.</p>
                        <p>"Services" means any services or loan facilities requested, made available or received via the Platform.</p>
                        <p>"Third Party Platforms" shall mean social networking platforms, such as Facebook, LinkedIn or other similar platforms.</p>
                        <p>“User Data” shall mean any data, information, documents or materials provided by you to the Company prior to or during the use of the Services.</p>
                        <p>By accessing or browsing any version of the Platform on any compatible device, you undertake that you have read, understood and agree to abide by these terms and conditions as amended from time to time, and any other applicable laws, whether or not you are a registered user of the platform. Please read the Terms and Conditions carefully as these form a legal agreement between you and Surecredit do not click “I Accept” or “I Agree” or download, install or use the App.</p>
                    </div>                  

                    <h4 class="h4 fsbold">Services</h4>

                    <p>You may apply for the Loan through the Platform, subject to the fulfilment of the eligibility criteria laid down in the Mobile App and website. You understand that the Company has been appointed by the Lender to collect, authenticate, track your location, verify and confirm the Personal Information or User Data, documents and details as may be required by the Lender to sanction the Loan.</p>

                    <p>You authorize the Company to collect and store the Personal Information through registration form available on the Platform. In order to avail the Services, you are required to register with the Company by creating your user account (“User Account”) or through Third Party Platforms and share and upload your Personal Information on the Platform. During the application process, you shall be required to provide and upload your Personal Information.</p>

                    <p>You agree that the Personal Information shall always be accurate, correct and complete. As part of the Services, you authorize us to import your details and Personal Information dispersed over Third Party Platforms. You understand and acknowledge that we may periodically request for updates on such Personal Information and we may receive such updated information from Third Party Platforms.</p>

                    <p>All transactions undertaken on your behalf by the Company will be on the basis of your express instructions/consent and will be strictly on a non- discretionary basis. You also authorise the Company to get your credit information report from one or more credit information companies as decided by the Company from time to time.</p>

                    <p>Once you verify and upload the Personal Information and/or other documents and details in the Platform, the Company shall process the same. Thereafter, the Company shall share your Loan application and documents with its Lending Partners in accordance with the applicable laws and the Privacy and Security Policy. The Lending Partners shall disburse the loan subject to the terms and conditions of the Loan Agreement.</p>

                    <p>You shall be notified in the event the Company/Lending Partner require any further Know Your Customer (“KYC”) documents/information from you. Accordingly, the Company/Lending Partner shall verify your KYC in their sole discretion.</p>

                    <p>Where a Lending Partner approves your Loan application, you shall be presented with the Loan Agreement on the Platform for you to execute. You may also be required to fill and upload any other document as may be required by the Lending Partner. </p>

                    <p>Upon successful execution of the Loan Agreement and/or other documents, the Loan shall be disbursed as per the mode provided in the Loan Agreement. You are required to repay the Outstanding Amount(s) to the Lender, on the respective due date(s) mentioned in the Loan Agreement/Platform. </p>

                    <p>Facilitating provision of loans to Users by various Lenders partnered with SureCredit. Assisting the Users with insurance offers from its partners. Provision of support to the Users in verifying the financial capabilities of Users</p>

                    <p>Any additional or duplicate payment, if any, made or caused by you will be refunded or cancelled, as the case may be, and would be credited to your account as per the Loan Agreement. </p>

                    <p>You understand and acknowledge that the Company reserves the right to track your location ("Track") during the provision of Services, and also in the event that you stop, cease, discontinue to use or avail the Services, through deletion or uninstallation of Mobile App or otherwise, till the event that your obligations to pay the Outstanding Amount(s) to LENDER exist. Deletion, uninstallation, discontinuation of our Services, shall not release you from the responsibility, obligation and liability to repay the Outstanding Amount(s). </p>

                    <p>You understand and acknowledge that you shall be solely responsible for all the activities that occur under your User Account while availing the Services. You undertake that the Company shall not be responsible and liable for any claims, damages, disputes arising out of use or misuse of the Services. By usage of the Services, you shall be solely responsible for maintaining the confidentiality of the User Account and for all other related activities under your User Account. The Company reserves the right to accept or reject your registration for the Services without assigning of any reason thereof.</p>

                    <p>You understand and acknowledge that, you are solely responsible for the capability of the electronic devices and the internet connection, you chose to run the Platform. The Platform’s operation or the Services on your electronic device is subject to availability of hardware, software specifications, internet connection and other features and specifications, required from time to time.</p>

                    <p>The User Data provided during the registration is stored by the Company for your convenience. You are not required to log-in to your account, every time, to use or access the Platform. You understand and acknowledge that by accepting the Terms and Conditions, you authorize us to track and fetch the User Data for the purpose of authentication and any updates with regards to your credentials.</p>

                    <h4 class="h4 fsbold">Eligibility</h4>

                    <p>The User of this Platform unequivocally declares and agrees that the User is a natural / legal person who has attained the age of at least 18 years old and are eligible to enter into a contract under the Indian Contract Act, 1872.</p>

                    <p>As a minor if you wish to use or transact on the Platform, such use or transaction may be made by your legal guardian or parents on the Platform. Surecredit reserves the right to terminate your membership and/or refuse to provide you with access to the Platform. </p>

                    <p>For accessing the App and using its services, you, as a User, further represent that you are an Indian national having tax residency in India. You also represent and assure that you are not a tax resident of any other country. In addition to these general requirements, you may also have to fulfil additional criteria to be able to use the Services available on the Platform.</p>

                    <p>If you violate any of the Terms and Conditions, Surecredit may terminate your membership, delete or suspend your account on the Platform and any related information and/or prohibit you from using or accessing the Platform at any time in its sole discretion.</p>

                     <h4 class="h4 fsbold">MODIFICATIONS AND UPDATES TO THE PLATFORM</h4>

                     <p>SureCredit reserves the right to temporarily or permanently modify or discontinue the Website, or any portion of the Website, for any reason, at our sole discretion, and without notice to you. SureCredit may also change the Terms of Use from time to time without notice to you. Such changes shall be effective immediately upon such posting.  Please review these Terms of Use from time to time, because your continued access or use of the Website after any modifications have become effective shall be deemed your conclusive acceptance of the modified Terms of Use.</p>

                     <h4 class="h4 fsbold">USER’S OBLIGATIONS FOR ACCESSING THE PLATFORM</h4>

                    <table class="table table-borderless">
                        <tr>
                            <td>1.</td>
                            <td>
                                <p>You may only use the Platform to avail the Services that may be offered on the Platform by Surecredit and the Lending Partners. You shall not use the Platform to make any fraudulent transactions. You agree not to use the Platform for any purpose that is unlawful, illegal or forbidden by the Terms and Conditions, or any applicable laws. Surecredit may, at its sole discretion, at any time and without prior notice or liability, impose additional requirements and restrictions or suspend, terminate or restrict your access to the Platform (or any portions thereof) if it comes to Surecredit’s notice that you have breached the Terms and Conditions, Privacy and Security Policy and/or any applicable laws.</p>
                            </td>
                        </tr>

                        <tr>
                            <td>2.</td>
                            <td>
                                <p>You must be the sole owner and User of your User Account. You cannot have more than one User Account at any point of time. You are responsible for maintaining the confidentiality and security of your User Account, password and activities that occur in or through your User Account and for restricting access to your device so as to prevent unauthorized access to your User Account.  </p>
                            </td>
                        </tr>

                         <tr>
                            <td>3.</td>
                            <td>
                                <p>The User must notify Surecredit at support@surecredit.in as soon as reasonably possible upon becoming aware of any actual or attempted unauthorised access to the Platform or any unauthorised transaction or attempt to execute an unauthorised transaction in connection with the Services or any other circumstance that might be reasonably likely to result in any prejudice to the security of the Platform or User Account login details.</p>
                            </td>
                        </tr>

                        <tr>
                            <td>4.</td>
                            <td>
                                <p>You agree, undertake and confirm not to host, display, upload, modify, publish, transmit, update or share any information that: </p>

                                 <table class="table table-borderless">
                                     <tr>
                                         <td>I</td>
                                         <td>
                                             <p>belongs to another person and to which you do not have any right to;</p>
                                         </td>
                                     </tr>
                                     <tr>
                                         <td>II</td>
                                         <td>
                                             <p>is grossly harmful, harassing, blasphemous, defamatory, obscene, pornographic, paedophilic, libellous, invasive of another's privacy, hateful, or racially, ethnically objectionable, disparaging, relating or encouraging money laundering or gambling, or otherwise unlawful in any manner whatsoever;</p>
                                         </td>
                                     </tr>
                                      <tr>
                                         <td>III</td>
                                         <td>
                                             <p>harm minors in any way;</p>
                                         </td>
                                     </tr>
                                      <tr>
                                         <td>IV</td>
                                         <td>
                                             <p>infringes any patent, trademark, copyright or other proprietary rights;</p>
                                         </td>
                                     </tr>
                                      <tr>
                                         <td>V</td>
                                         <td>
                                             <p>violates any law for the time being in force;</p>
                                         </td>
                                     </tr>
                                      <tr>
                                         <td>VI</td>
                                         <td>
                                             <p>deceives or misleads the addressee about the origin of such messages or communicates any information which is grossly offensive or menacing in nature;</p>
                                         </td>
                                     </tr>
                                      <tr>
                                         <td>VII</td>
                                         <td>
                                             <p>impersonate another person;</p>
                                         </td>
                                     </tr>
                                      <tr>
                                         <td>VIII</td>
                                         <td>
                                             <p>contains software viruses or any other computer code, files or programs designed to interrupt, destroy or limit the functionality of any computer resource;</p>
                                         </td>
                                     </tr>
                                      <tr>
                                         <td>IX</td>
                                         <td>
                                             <p>threatens the unity, integrity, defence, security or sovereignty of India, friendly relations with foreign states, or public order or causes incitement to the commission of any cognizable offence or prevents investigation of any offence or is insulting any other nation.</p>
                                         </td>
                                     </tr>
                                 </table>
                            </td>
                        </tr>

                        <tr>
                            <td>5.</td>
                            <td>
                                <p>You further agree, undertake and confirm not to: </p>

                                 <table class="table table-borderless">
                                     <tr>
                                         <td>I</td>
                                         <td>
                                             <p>use the Platform or the Services for committing fraud, embezzlement, money laundering, or for any unlawful and/or illegal purposes;</p>
                                         </td>
                                     </tr>
                                     <tr>
                                         <td>II</td>
                                         <td>
                                             <p>to reproduce, duplicate, copy, sell, resell or exploit any portion of the Platform;</p>
                                         </td>
                                     </tr>
                                      <tr>
                                         <td>III</td>
                                         <td>
                                             <p>use the Platform to harm or injure any third party;</p>
                                         </td>
                                     </tr>
                                      <tr>
                                         <td>IV</td>
                                         <td>
                                             <p>forge headers or otherwise manipulate identifiers in order to disguise the origin of any content transmitted through the Platform;</p>
                                         </td>
                                     </tr>
                                      <tr>
                                         <td>V</td>
                                         <td>
                                             <p>upload, post, email, transmit or otherwise make available any content that you do not have a right to make available under any law or under contractual or fiduciary relationships (such as inside information, proprietary and confidential information learned or disclosed as part of employment relationships or under non-disclosure agreements);</p>
                                         </td>
                                     </tr>
                                      <tr>
                                         <td>VI</td>
                                         <td>
                                             <p>upload, post, email, transmit or otherwise make available on the Platform, any unsolicited or unauthorized advertising, promotional materials, "junk mail," "spam", "chain letters," "pyramid schemes," or any other form of solicitation;</p>
                                         </td>
                                     </tr>
                                      <tr>
                                         <td>VII</td>
                                         <td>
                                             <p>interfere with or disrupt the Platform or servers or networks connected to the Platform, or disobey any requirements, procedures, policies or regulations of networks connected to the Platform.</p>
                                         </td>
                                     </tr>                                      
                                 </table>
                            </td>
                        </tr>

                        <tr>
                            <td>6.</td>
                            <td>
                                <p>We reserve the right, but have no obligation, to monitor the materials posted on the Platform. Notwithstanding this right, you remain solely responsible for the content of the materials you post on the Platform. In no event shall Surecredit assume or have any responsibility or liability for any content posted or for any claims, damages or losses resulting from use of content and/or appearance of content on the Platform. </p>
                            </td>
                        </tr>                       
                    </table>

                    <h4 class="h4 fsbold">CONTENT AVAILABLE</h4>

                    <p>You may not use the Website or any content for any purpose that is unlawful or prohibited by these Terms of Use, or to solicit the performance of any illegal activity or other activity which infringes the rights of SureCredit and / or others.</p>

                    <p>The Content contained in this Website, including text, graphics, links or other terms are provided on an “as is”, “as available” basis and are protected by copyright in favour of Operator. User should not distribute text or graphics to others without the express written consent of the Operator. User should not copy, download, publish, distribute or reproduce any of the Content contained on this Website in any form without prior permission of the Operator. </p>

                    <p>Subject to the Terms and Conditions mentioned herein SureCredit shall also have the liberty to remove any objectionable content which is in contravention of the Terms and Conditions herein or share such information with any governmental authority as per procedures laid down by the law for the time being in force in India. </p>

                     <h4 class="h4 fsbold">INTELLECTUAL PROPERTY RIGHTS</h4>

                     <p>You understand, acknowledge and agree that the Company is the sole owner of all rights, title and interest, including any and all intellectual property rights in the Content, Platform, Services, logos, trade names, brand names, designs and any necessary software used in connection with the Platform.</p>

                     <p>There may be proprietary logos, service marks and trademarks found on the Platform whether owned/used by the Company or otherwise. By displaying them on the Platform, the Company is not granting you any license to utilize the proprietary logos, service marks, or trademarks. Any unauthorized use of the same may violate applicable intellectual property laws.</p>

                     <p>You understand and acknowledge that the Platform is owned by the Company. Nothing under these Terms shall be deemed to be a transfer in ownership, rights, title, from the Company to you or any third party, in the Platform. You are entitled to avail the Services offered by the Company during the validity of your registration with the Company.</p>

                     <p>SureCredit grants You a limited license to access and use the Platform for availing the Services, but not to download any material from it. Any unauthorized access to the Platform or any networks, servers or computer systems connected to Platform and any attempt to modify, adapt, translate or reverse engineer any part of the Platform or re-format or frame any portion of the pages of the Platform, save to the extent expressly permitted by these Terms & Conditions, is not permitted. This license is non-transferable and does not permit any resale or commercial use of this Platform or its contents; any downloading or copying of account information for the benefit of anyone other than Your use</p>

                    <h4 class="h4 fsbold">THIRD PARTY LINKS IN THE PLATFORM</h4>

                     <p>The Platform may contain links to other websites owned and operated by third parties who are not related to the Platform ("Linked Websites"). SureCredit has no control over and accepts no responsibility for the content of any website or mobile application to which a link on the Platform exists. Such linked websites and mobile applications are provided “as is” for User’s convenience only with no warranty, express or implied, for the information provided therein. SureCredit does not provide any endorsement or recommendation of any third-party website or mobile application to which the Platform provides a link.</p>

                     <p>Your access or use of such Linked Website is entirely at your own risk. The Company shall not be a party to any transaction between you and the Linked Website. Your use of a Linked Website is subject to the terms and conditions of that respective Linked Website.</p>

                     <p>The Platform may also contain third party advertisements, if any. The display of such advertisements does not in any way imply an endorsement or recommendation by/of the relevant advertiser, its products or services. Further, the Users consent and agree that the content provided in the Website and the App shall be synced and shall be available in both mediums.</p>

                      <h4 class="h4 fsbold">ANCILLARY SERVICES</h4>
                      
                      <p>You may get access to chat rooms, blogs, feedbacks, reviews and other features ("Ancillary Services") that are/may be offered from time to time on the Platform and may be operated by us or by a third party on our behalf. You shall not (nor cause any third party to) use these Ancillary Services to perform any illegal activities (including without limitation defaming, abusing, harassing, stalking, threatening, promoting racism, or otherwise violating the legal rights, such as rights of privacy, of others) or immoral activities, falsely stating or otherwise misrepresenting your affiliation with a person or entity.</p>

                      <p>Additionally, the Platform may contain advice/opinions and statements of various professionals/ experts/ analysts, etc. The Company does not endorse the accuracy, reliability of any such advices/opinions/ and statements. You may rely on these, at your sole risk and cost. You shall be responsible for independently verifying and evaluating the accuracy, completeness, reliability and usefulness of any opinions, services, statements or other information provided on the Platform.</p>

                      <p>All information or details provided on the Platform shall not be interpreted or relied upon as legal, accounting, tax, financial, investment or other professional advice, or as advice on specific facts or matters. The Company may, at its discretion, update, edit, alter and/or remove any information in whole or in part that may be available on the Platform and shall not be responsible or liable for any subsequent action or claim, resulting in any loss, damage and or liability. Nothing contained herein is to be construed as a recommendation to use any product or process, and the Company makes no representation or warranty, express or implied that, the use thereof will not infringe any patent, or otherwise.</p>

                       <h4 class="h4 fsbold">SUSPENSION/TERMINATION OF SERVICES</h4>
                       <p>The Company reserves its rights to terminate or suspend your User Account in the event:</p>

                    
                    <table class="table table-borderless">
                        <tr>
                            <td>I</td>
                            <td>
                                <p>You breach any provision of the Terms and Conditions and the Privacy and Security Policy</p>
                            </td>
                        </tr>

                        <tr>
                            <td>II.</td>
                            <td>
                                <p>The Company is required to do so under applicable law  </p>
                            </td>
                        </tr>

                         <tr>
                            <td>III.</td>
                            <td>
                                <p>The Company chooses to discontinue the Services being offered or discontinue to operate the Platform  </p>
                            </td>
                        </tr>

                        <tr>
                            <td>IV.</td>
                            <td>
                                <p>The license granted to use the Platform expires</p>
                            </td>
                        </tr>

                        <tr>
                            <td>V.</td>
                            <td>
                                <p>Of non-payment of Outstanding Amount(s) by you to the Lender </p>
                            </td>
                        </tr>                                           
                    </table>

                    <p>The Company reserves its right to track you, even when you have uninstalled the App, until all your obligations, including but not limited to payment of the Outstanding Amount(s) is in subsistence.</p>

                    <p>SureCredit may stop providing the Services and may terminate use of it at any time without giving notice of termination to You. Unless SureCredit informs the User otherwise, upon any termination, (a) the rights and licenses granted to You in these terms will end; and (b) User must stop using the Platform forthwith. SureCredit reserves the right to suspend or cease providing any Service and shall have no liability or responsibility to the User in any manner whatsoever if it chooses to do so.</p>

                     <h4 class="h4 fsbold">DISCLAIMER OF WARRANTIES </h4>

                     <p>Surecredit operates the Platform as an intermediary and it hereby expressly disclaims all liability for the Services provided by the Lending Partners through the Platform, or any liability arising out of your Loan Agreements with Lending Partners.</p>

                     <p>The User acknowledges and confirms that he/she is well aware of the rules & regulations and the risks involved in services offered by SureCredit. Changes are periodically added to the information herein.</p>

                     <p>You agree that your use of the Platform is being conducted voluntarily and after due research about the Platform and the Lending Partners. Surecredit is not responsible for any adverse consequences arising out your use of the Platform if such consequences are not traceable to any breach on the part of Surecredit.</p>

                     <p>You expressly understand and agree that:</p>


                     <table class="table table-borderless">
                        <tr>
                            <td>a)</td>
                            <td>
                                <p>The Platform, all Content and Services included on or otherwise made available to you through the Platform are provided on an “as is” and on an “as available” basis, without any representation or warranties, express or implied except otherwise specified in writing. Surecredit expressly disclaims all warranties of any kind, whether express or implied, including, but not limited to, the implied warranties of merchantability, fitness for a particular purpose, title, non-infringement, and security and accuracy, as well as all warranties arising by usage of trade, course of dealing, or course of performance.  </p>
                            </td>
                        </tr>

                        <tr>
                            <td>b)</td>
                            <td>
                                <p>	Your use of the Services and the Platform is at your sole risk. </p>
                            </td>
                        </tr>

                         <tr>
                            <td>c)</td>
                            <td>
                                <p>	You understand and agree that if you use, access, download, or otherwise obtain information, materials, or data through the App or website, the same shall be at your own discretion and risk and that you will be solely responsible for any damage to your property (including your computer system and/or other device) or loss of data that results from the download or use of such material or data No advice or information, whether verbal or written, obtained by you from Surecredit for the Services or through the Platform shall create any warranty not expressly stated in the Terms and Conditions.  </p>
                            </td>
                        </tr>

                        <tr>
                            <td>d)</td>
                            <td>
                                <p>	Surecredit makes no warranty, including implied warranty, and expressly disclaims any obligation, that: </p>

                                 <table class="table table-borderless">
                                     <tr>
                                         <td>i</td>
                                         <td>
                                             <p>The Contents are and will be complete, exhaustive, accurate or suitable to your requirements</p>
                                         </td>
                                     </tr>
                                     <tr>
                                         <td>ii</td>
                                         <td>
                                             <p>The Platform or the Services will meet your requirements or will be available on an uninterrupted, timely, secure, or error-free basis</p>
                                         </td>
                                     </tr>
                                      <tr>
                                         <td>iii</td>
                                         <td>
                                             <p>The results that may be obtained from the use of the Platform or Services will be accurate or reliable./p>
                                         </td>
                                     </tr>
                                 </table>
                            </td>
                        </tr>

                    </table>

                     <h4 class="h4 fsbold">INDEMNITY</h4>
                     <p>You agree to indemnify and hold the Company, and its subsidiaries, affiliates, officers, agents, co-branders or other partners, and employees, harmless from and against any costs, claims, charges, expenses, losses, damages or demand, including attorneys’ fees, made by any third party due to or arising out of: </p>

                      <table class="table table-borderless">
                        <tr>
                            <td>I.</td>
                            <td>
                                <p>your violation of these Terms </p>
                            </td>
                        </tr>

                        <tr>
                            <td>II.	</td>
                            <td>
                                <p>your use of the App </p>
                            </td>
                        </tr>

                         <tr>
                            <td>III.</td>
                            <td>
                                <p>	your violation of any rights of other users of the Platform</p>
                            </td>
                        </tr>

                        <tr>
                            <td>IV.</td>
                            <td>
                                <p>	your use or misuse of the Platform or the Services </p>
                            </td>
                        </tr>

                        <tr>
                            <td>V.</td>
                            <td>
                                <p>	your violation of applicable laws</p>
                            </td>
                        </tr>

                        <tr>
                            <td>VI.</td>
                            <td>
                                <p>	your breach of the terms of your contract with lenders</p>
                            </td>
                        </tr>

                    </table>

                     <h4 class="h4 fsbold">LIMITATIONS OF LIABILITY </h4>
                   
                      <table class="table table-borderless">
                        <tr>
                            <td>a)</td>
                            <td>
                                <p>	You expressly understand and agree that the Company, including its directors, officers, employees or representatives shall not be liable for any direct, indirect, incidental, special, consequential or exemplary damages, including but not limited to, damages for loss of profits, goodwill, use, data or other intangible losses (even if the Company has been advised of the possibility of such damages), resulting from; (a) use or the inability to avail the Services (b) inability to use the Platform (c) failure or delay in providing the Services or access to the Platform (d) any performance or non-performance by the Company (e) any damages to or viruses that may infect your electronic devices or other property as the result of your access to the Platform or your downloading of any content from the Platform and (f) server failure or otherwise or in any way relating to the Services.</p>
                            </td>
                        </tr>

                        <tr>
                            <td>b)</td>
                            <td>
                                <p>	In no event shall Company’s total cumulative liability arising from or relating to these Terms and Conditions exceed the amount of fees received from the User by the Company.</p>
                            </td>
                        </tr>

                         <tr>
                            <td>c)</td>
                            <td>
                                <p>	This limitation of liability clause shall prevail over any conflicting or inconsistent provision contained in any of the documents / content comprising this Terms & Conditions. It is up to You to take precautions to ensure that whatever You select for Your use is free of such items as viruses, worms, malware, Trojan horses and other items of a destructive nature. </p>
                            </td>
                        </tr>                      
                    </table>

                    <h4 class="h4 fsbold">FORCE MAJEURE </h4>
                     <p>Without limiting the foregoing, under no circumstances shall the SureCredit be held not liable for any damage, loss, loss of services of  Platform, due to deficiency in provision of the Services resulting directly or indirectly from acts of nature, forces, or causes beyond its reasonable control, including, without limitation, internet failures, computer equipment failures, telecommunication equipment failures, change in applicable regulations, or any other government regulations, floods, storms, electrical failure, civil disturbances, riots.</p>

                    <h4 class="h4 fsbold">PRIVACY POLICY</h4>

                     <table class="table table-borderless">
                        <tr>
                            <td>VI.</td>
                            <td>
                                <p>	This Website’s Privacy Policy governs our collection, use, and disclosure of your personal information and is incorporated into these User T & C. Please read it carefully. It describes what information we collect from you and when, how and why we may create an account for you, who we share your information with and when and how you can opt-out or delete your account. This is important information. By providing your personal details and by using our website, you consent to our Privacy Policy</p>
                            </td>
                        </tr>

                        <tr>
                            <td>VII.</td>
                            <td>
                                <p>		You acknowledge and agree that you cannot withdraw your consent to the Privacy and Security Policy during the validity and subsistence of a Loan Agreement, to the extent such consent is necessary for the consummation of the transactions under the Loan Agreement.</p>
                            </td>
                        </tr>                                  
                    </table>

                     <h4 class="h4 fsbold">GOVERNING LAW & JURISDICTION</h4>

                     <p>These User T & C shall be a legally binding agreement between User and us and shall be governed by and interpreted and construed in accordance with the laws of India. Notwithstanding the foregoing, we reserve the right to pursue any action or claim against you in any court of competent jurisdiction which we believe is the most appropriate to seek any relief. </p>

                      <h4 class="h4 fsbold">MISCELLANEOUS </h4>

                      <table class="table table-borderless">
                        <tr>
                            <td>a)</td>
                            <td>
                                <p>	Each of the provisions of the Terms and Conditions is severable from the others and if one or more of them becomes void, illegal or unenforceable, the remainder will not be affected in any way. </p>
                            </td>
                        </tr>

                        <tr>
                            <td>b)</td>
                            <td>
                                <p>Notwithstanding that the whole or any part of any provision of these Terms may prove to be illegal or unenforceable, the other provisions of these Terms and the remainder of the provision in question shall continue in full force and effect.</p>
                            </td>
                        </tr>        

                        <tr>
                            <td>c)</td>
                            <td>
                                <p>	The rights of Surecredit, its affiliates, associates and agents under the Terms and Conditions may be exercised as often as necessary and are cumulative and not exclusive of their rights under any applicable law. Any delay in the exercise or non-exercise of any such right is not a waiver of that right. 	</p>
                            </td>
                        </tr>    
                        <tr>
                            <td>d)</td>
                            <td>
                                <p>	Surecredit may assist or co-operate with authorities in any jurisdiction in relation to any direction or request to disclose personal or other information regarding any User or the use of the Platform, Content or the Services. 	</p>
                            </td>
                        </tr> 
                        <tr>
                            <td>e)</td>
                            <td>
                                <p>	No third party shall have any rights to enforce any Terms contained herein.		</p>
                            </td>
                        </tr> 
                        <tr>
                            <td>f)</td>
                            <td>
                                <p>	You shall not licence, sell, transfer or assign your rights, obligations, or covenants under these Terms in any manner without our prior written consent. 		</p>
                            </td>
                        </tr> 
                        <tr>
                            <td>g)	</td>
                            <td>
                                <p>We may grant or withhold this consent in its sole discretion and subject to any conditions it deems appropriate. We may assign its rights to any of its affiliates, subsidiaries, or parent companies, or to any successor in interest of any business associated with the Services without any prior notice to you.		</p>
                            </td>
                        </tr>                        
                    </table>

                     <h4 class="h4 fsbold">GRIEVANCES </h4>
                     <p>Users have complete authority to file complaint/ share feedback if they are disappointed by services rendered by SureCredit. They can give their complaint/ feedback in writing or by way of an email to the following:  support@surecredit.in</p>
                     
               </div>
               <!--/ container -->             
            </div>
            <!--/ sub page Body Content-->
        </section>
        <!--/ subpage body -->
    </main>
    <!--/ main --> 
    <?php include 'includes/footer.php' ?> 
    <?php include 'includes/scripts.php'?>
</body>

</html>