var swiper = new Swiper('.homeSwiper', {
    slidesPerView: 1,
    spaceBetween: 30,
    keyboard: {
        enabled: true,
    },
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    autoplay: {
        delay: 4000,
    },
});




//click event to move top
$(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
        $('#moveTop').fadeIn();
    } else {
        $('#moveTop').fadeOut();
    }
});

//click event to scroll top
$('#moveTop').click(function () {
    $('html, body').animate({
        scrollTop: 0
    }, 'fast')
});

//on scroll add class to header 
$(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
        $('.fixed-top').addClass('fixed-theme');
    } else {
        $('.fixed-top').removeClass('fixed-theme');
    }
});


document.onreadystatechange = function () {
    var state = document.readyState
    if (state == 'complete') {
        setTimeout(function () {
            document.getElementById('load').style.visibility = "hidden";
        }, 1000);
    }
}