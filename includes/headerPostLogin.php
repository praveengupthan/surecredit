  <div id="load"></div>
  <!-- header -->
    <header class="fixed-top">
        <div class="containerCustom">
            <div class="navbar navbar-expand-xl bsnav"><a class="navbar-brand" href="index.php">
                    <img src="img/logo.svg" alt="">
                </a>
                <button class="navbar-toggler toggler-spring"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse">
                    <ul class="navbar-nav navbar-mobile mr-0">
                        <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='index.php'){echo'active';}else {echo'nav-link';}?>" href="index.php">Home</a></li>
                        <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='personalLoans.php'){echo'active';}else {echo'nav-link';}?>" href="personalLoans.php">Personal Loans</a></li>
                        <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='howitworks.php'){echo'active';}else {echo'nav-link';}?>" href="howitworks.php">How it Works</a></li>
                        <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='about.php'){echo'active';}else {echo'nav-link';}?>" href="about.php">About us</a></li>
                        <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='faq.php'){echo'active';}else {echo'nav-link';}?>" href="faq.php">Faq's</a></li>
                        <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='contact.php'){echo'active';}else {echo'nav-link';}?>" href="contact.php">Contact</a></li>
                    </ul>
                    <ul class="navbar-nav navbar-mobile ms-auto">
                        <li class="nav-item"><a class="nav-link applybtn" href="userApplyLoan.php">Apply Loan</a></li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" 
                                aria-controls="offcanvasRight">Praveen.. <span class="icon-navbar icomoon"></span></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="bsnav-mobile">
                <div class="bsnav-mobile-overlay"></div>
                <div class="navbar"></div>
            </div>
        </div>
    </header>
    <!--/ header -->
    <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasRight" aria-labelledby="offcanvasRightLabel">
        <div class="offcanvas-header">
            <h5 id="offcanvasRightLabel">Praveen Kumar N</h5>
            <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <div class="offcanvas-body">
            <div class="profileNav">
                <div class="userProfilePic text-center">
                    <span class="icon-user icomoon"></span>
                </div>
                <ul class="userNav">
                    <li><a href="userDashboard.php">Dashboard</a></li>
                    <li><a href="userLoans.php">My Loans</a></li>                    
                    <li><a href="userkycDocuments">Documents</a></li>                                       
                    <li><a href="userRepayLoan">Repay Loan</a></li>
                </ul>  
                <a href="index.php" class="d-block w-100 btnCustom">Logout</a>              
            </div>
        </div>
    </div>