<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sure Credit Faq's</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/fav.png"> 
    <?php include 'includes/styles.php'?>
    <?php include 'includes/data.php'?>
</head>

<body> <?php include'includes/header.php' ?>
    <!-- main -->
    <main class="subpageMain">
        <!-- subpage header -->
        <section class="subpageHeader">
            <!-- container -->
            <div class="container">
                <article>
                    <h6 class="h6">Frequently Asked Questions</h6>
                    <h1>FAQ's</h1>
                </article>
            </div>
            <!--/ container -->
        </section>
        <!--/ sub page header -->

        <!-- subpage body -->
        <section class="subpageBody">
            <!-- container -->
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>                        
                        <li class="breadcrumb-item active" aria-current="page">FAQ's</li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
            <!-- sub page Body Content -->
            <div class="subpageBodyContent">
               <!-- container -->
               <div class="container faqcontainer">
                    <div class="Customtabs">
                        <!-- accordion -->
                        <div class="accordion-container">
                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">How to get an Instant Personal loan from SureCredit</h2>
                                <div class="ac-a">
                                    <p>Getting <a href="javascript:void(0)" target="_blank">instant personal loan </a>at SureCredit is quite effortless and easy, since we have integrated AI to both website and mobile application to facilitate applicants with seamless loan borrowing experience. Apply Now!!</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">How can I apply for an Instant Personal loan without documents in India?</h2>
                                <div class="ac-a">
                                    <p>No. Minimum documentation required. You can visit SureCredit.in or Download the SureCredit App from the Google Play Store for more info.</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">How easy is it to get an instant cash loan from SureCredit?</h2>
                                <div class="ac-a">
                                    <p>It is extremely easy to get <a href="javascript:void(0)" target="_blank"> instant cash loan </a> at SureCredit in simple steps</p>
                                    <ul class="listItems">
                                        <li>Form Filling</li>
                                        <li>Upload Documents</li>
                                        <li>Instant Approval</li>
                                        <li>Immediate Disbursal</li>
                                    </ul>
                                    <p>The instant cash loan application process is very quick and SureCredit approves the loan without any physical verification. There are no hidden charges on the loan approval process.</p>
                                </div>
                            </div>
                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">How can I get an Instant Personal Loan in 1 hour?</h2>
                                <div class="ac-a">
                                    <p>With SureCredit app, accessing <a href="javascript:void(0)">instant personal loan </a>is now a matter of minutes.</p>
                                </div>
                            </div>

                                <div class="ac">
                                <h2 class="ac-q" tabindex="0">Can I apply for instant personal loan through SureCredit online loan app?</h2>
                                <div class="ac-a">
                                    <p>Yes. With SureCredit online loan app, you can access instant personal loan assistance whenever and wherever the need for financial assistance persists. Click here to <a href="javascript:void(0)" target="_blank"> download the app now.</a></p>
                                </div>
                            </div>

                                <div class="ac">
                                <h2 class="ac-q" tabindex="0">Which is the best Instant Personal Loan app in India?</h2>
                                <div class="ac-a">
                                    <p>SureCredit is ought to be the best online loan app to seek personal loan in india. We provide instant loan approval facility and flexible loan repayment options with no hidden charges. For more information, contact us.</p>
                                </div>
                            </div>

                                <div class="ac">
                                <h2 class="ac-q" tabindex="0">How safe Cash loans Online are?</h2>
                                <div class="ac-a">
                                    <p>Cash loans online are safe, if you seek monetary support from a genuine lender. It’s quite simple to gauge the trustworthiness of any lender just by simply check their overall borrowing record and customers' feedback.</p>
                                </div>
                            </div>

                                <div class="ac">
                                <h2 class="ac-q" tabindex="0">How to Apply for an Advance Salary Loan in India at SureCredit?</h2>
                                <div class="ac-a">
                                    <p>Download the SureCredit app, register yourself with requested personal and professional details to help us gauge your eligibility. </p>
                                    <ul class="listItems">
                                        <li>If your eligibility is confirmed, upload mandatory documents through mobile for salary advance loan approval. </li>
                                        <li>Approved loan amount will be disbursed directly into your bank account.</li>
                                    </ul>
                                    <p>For more information, <a href="javascript:void(0)" target="_blank">Click Here.</a></p>
                                </div>
                            </div>

                                <div class="ac">
                                <h2 class="ac-q" tabindex="0">How does Personal Line of Credit Works?</h2>
                                <div class="ac-a">
                                    <p>A line of credit is a preset amount of money that a financial institution like a bank or credit union has agreed to lend you. You can draw from the line of credit when you need it, up to the maximum amount as per your limit. You'll pay interest on the amount you used.</p>
                                </div>
                            </div>

                                <div class="ac">
                                <h2 class="ac-q" tabindex="0">How to get Instant Cash Loan in 1 Hour?</h2>
                                <div class="ac-a">
                                    <p>To get an Instant cash loan you need to download the SureCredit App from Google Play and login with your credentials and upload the KYC  and other loan required documents.  Post which our team will analyze your credentials based on your documents submitted to us.</p>
                                </div>
                            </div>

                                <div class="ac">
                                <h2 class="ac-q" tabindex="0">How to get an Instant Loan Without Documents?</h2>
                                <div class="ac-a">
                                    <p>No. Minimum documents required. Lenders who offer instant loan approvals require only the basic documentation like – PAN Card, ID proof, and address proof and salary proof which are mandatory in order to process the loan.</p>
                                </div>
                            </div>

                                <div class="ac">
                                <h2 class="ac-q" tabindex="0">Am I eligible to apply for a Salary Advance Loan at SureCredit.in?</h2>
                                <div class="ac-a">
                                    <p>To be eligible to apply for a <a href="javascript:void(0)" target="_blank">Salary Advance loan </a>at SureCredit, you should be</p>

                                    <ul class="listItems">
                                        <li>Aged above 21 </li>
                                        <li>An Indian Citizen</li>
                                        <li>A working professional</li>
                                    </ul>
                                    <p>To know more about the eligibility, <a href="javascript:void(0)" target="_blank">ClickHere</a></p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">How to apply for a salary advance loan through SureCredit.in?</h2>
                                <div class="ac-a">
                                    <p>Download SureCredit app, register yourself with requested personal and professional details to help us gauge your eligibility. </p>
                                    <ul class="listItems">
                                        <li>If your eligibility is confirmed, upload mandatory documents through mobile for salary advance loan approval. </li>
                                        <li>Approved loan amount will be disbursed directly into your bank account.</li>
                                    </ul>
                                    <p>For more information, <a href="javascript:void(0)">Click Here.</a></p>
                                </div>
                            </div>

                             <div class="ac">
                                <h2 class="ac-q" tabindex="0">What is SureCredit?</h2>
                                <div class="ac-a">
                                    <p>SureCredit is an Instant Personal Loan Platform and India’s trusted app for individuals. SureCredit acts as a technology intermediary and facilitates this Personal Loan from lenders to individuals. The documentation is also very minimal and the entire process will took 15 minutes because the application process is completely online, and on approval, the cash is immediately transfer to user account. Moreover we Provide loans for up to Rs 1,50,000/- with easy and affordable instalment options. Visit <a href="javascipt:void(0)">our Website</a> to learn more.</p>
                                    <p>I am facing technical issues during profile completion. What do I od? Send us the error screen shot, along with your registered phone number and SCID on support@surecredit.in</p>
                                </div>
                            </div>

                             <div class="ac">
                                <h2 class="ac-q" tabindex="0">Why should I take loan from SureCredit?</h2>
                                <div class="ac-a">
                                    <p>SureCredit is one stop solution for all your financial needs. At SureCredit, Quick credit assessment ensures fast disbursal of a loan at affordable rates and flexible terms.</p>
                                </div>
                            </div>

                             <div class="ac">
                                <h2 class="ac-q" tabindex="0">How does SureCredit works?</h2>
                                <div class="ac-a">
                                    <p>Applying for a Personal Loan through the SureCredit Platform is simple, quick and easy. Just follow  the below steps:  </p>
                                    <ul class="listItems">
                                        <li>Install the SureCredit app from the Play Store.</li>
                                        <li>Register yourself through your Facebook, Google</li>
                                        <li>Fill in your basic details to check your eligibility.</li>
                                        <li>Upon confirmation on the eligibility, you will need to upload your KYC documents fill out your personal information.</li>
                                        <li>Upload photos of your documents.</li>
                                        <li>Provide your bank account details.</li>
                                        <li>Once verified from our end you’ll get the money directly in your bank account.</li>
                                    </ul>                                    
                                </div>
                            </div>

                             <div class="ac">
                                <h2 class="ac-q" tabindex="0">Can I send my documents to you over email?</h2>
                                <div class="ac-a">
                                    <p>As of now, we support uploading of documents through the app only</p>
                                </div>
                            </div>

                             <div class="ac">
                                <h2 class="ac-q" tabindex="0">On what basis does SureCredit lend?</h2>
                                <div class="ac-a">
                                    <p>SureCredit lends based on the Credibility of the Borrower and provided documents.</p>
                                </div>
                            </div>

                             <div class="ac">
                                <h2 class="ac-q" tabindex="0">What is SureCredit Terms and Conditions?</h2>
                                <div class="ac-a">
                                    <p>You can access the <a href="javascript:void(0)" target="_blank">Terms and Conditions</a> </p>
                                </div>
                            </div>

                             <div class="ac">
                                <h2 class="ac-q" tabindex="0">What is SureCredit Privacy Policy?</h2>
                                <div class="ac-a">
                                    <p>You can access the <a href="javascript:void(0)">Privacy Policy</a></p>
                                </div>
                            </div>

                             <div class="ac">
                                <h2 class="ac-q" tabindex="0">What are the advantages of availing an instant personal loan through SureCredit over a personal loan from bank?</h2>
                                <div class="ac-a">
                                    <p>Since our loan process is completely online and paperless, we are able to process your loan much faster and eliminate many of the costs that are incurred at banks and large instantly financial institutions.</p>
                                </div>
                            </div>

                             <div class="ac">
                                <h2 class="ac-q" tabindex="0">What all-purpose the SureCredit personal loan can cover?</h2>
                                <div class="ac-a">
                                    <p>SureCredit personal loan can be used to meet the money/financial requirements for any, kind of personal use. Like: Shopping, Weddings, Gifts, and Medical, Travel etc.</p>
                                </div>
                            </div>

                             <div class="ac">
                                <h2 class="ac-q" tabindex="0">Is it necessary to install the SureCredit app to apply for a loan?</h2>
                                <div class="ac-a">
                                    <p>Yes, you need to install the SureCredit app on your Android mobile phone to apply for a Personal Loan.</p>
                                </div>
                            </div>

                             <div class="ac">
                                <h2 class="ac-q" tabindex="0">Do I need to be employed to be able to apply for loan in SureCredit?</h2>
                                <div class="ac-a">
                                    <p>Yes, it is mandatory for you to have a regular source of income to be able to avail a loan from SureCredit</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">How do I register?</h2>
                                <div class="ac-a">
                                    <p>Download the SureCredit app from the Play Store & register yourself through your Phone Number, Facebook, Gmail or LinkedIn account. </p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">I do not have a Facebook, Gmail or LinkedIn account. Can I still use the SureCredit app?</h2>
                                <div class="ac-a">
                                    <p>No, an active Facebook, Gmail or LinkedIn account is mandatory for signing up with SureCredit.</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">I haven’t received the OTP. What can be the reason?</h2>
                                <div class="ac-a">
                                    <p>The SMS is delivered by your network operator, so network issues can prevent you from receiving the OTP. You can try switching off and restarting your phone. Or else, you can try after some time.</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">I have uploaded all the required Documents still I have received a mail asking for some documents?</h2>
                                <div class="ac-a">
                                    <p>Our internal team sends out emails to you only if there’s an issue with your document you have upload or for any pending documents. Please go through the email you’ve received for more details on what documents we require from you again.</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">I Don't Have Any Docs For Current Address Proof. Still can I apply for the loan?</h2>
                                <div class="ac-a">
                                    <p>We would not be able to process your application without any current address proof. Please try to arrange an address proof which is listed by us to ensure faster loan approval.</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">Do I need to upload KYC documents every time I take a loan through SureCredit?</h2>
                                <div class="ac-a">
                                    <p>No, the documents are not required to be provided again and again. once your documents have been successfully verified and your SureCredit profile is confirmed, you need not re-submit your documents. However, you are required to notify us and upload new documents for verification in case there are any changes in your employment.</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">How safe are my uploaded KYC documents?</h2>
                                <div class="ac-a">
                                    <p>KYC is a mandatory requirement in accordance with RBI regulations. We ask for minimal documents for your KYC process. All your personal and financial information are encrypted using SSL protocol. So all your information is completely safe with us.</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">I haven’t received documents like loan agreement, Sanction letter. What should I do?</h2>
                                <div class="ac-a">
                                    <p>Ideally, you should have received your documents on your registered email address at the time of loan disbursement. However, in case you haven't received them, you may write to us at <a href="mailto:support@SureCredit.in">support@SureCredit.in </a>and quote your loan account number to receive assistance.</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">Who is eligible for to apply for a loan from SureCredit?</h2>
                                <div class="ac-a">
                                    <p>Any Indian citizen residing in India, above the age of 21 with a proof of current employment, earning a monthly salary of ₹25,000 and above, a bank account and a valid identity and address proof can apply.</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">I am a self-employed customer. Can I still avail a personal loan with SureCredit?</h2>
                                <div class="ac-a">
                                    <p>We're currently unable to process loans for our self-employed customers. We'll update you as soon as we start processing applications.</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">Will loan get approved, if get my salary in cash?</h2>
                                <div class="ac-a">
                                    <p>At this moment, we only give loans to salaried individuals who get their salary via bank transfer – directly from their employer.</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">I have one current loan running with SureCredit. Can I apply for another loan?</h2>
                                <div class="ac-a">
                                    <p>Yes, you can apply.</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">Is there requirement of collateral/security for this Loan?</h2>
                                <div class="ac-a">
                                    <p>No, you don't need any collateral/security for this loan.</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">Which location can you apply for a loan through SureCredit?</h2>
                                <div class="ac-a">
                                    <p>SureCredit is a digital platform and is available across all cities in India.</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">My CIBIL score is bad, am I eligible for loan?</h2>
                                <div class="ac-a">
                                    <p>Yes, our underwriting is based on machined credit process where we check customer’s multiple data points. Loan application can be considered if your profile qualifies our underwriting parameters.</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">I am eligible for Personal Loan with SureCredit, but cannot avail a loan of 1 Lakh, Why?</h2>
                                <div class="ac-a">
                                    <p>Mostly customers on SureCredit start out by being eligible for Flexi Personal Loan first. As you make timely repayments of your loan, there is a good possibility that your maximum disbursal amount will increase over time it will gives more confidence to our lending partners in your repayment capacity. However, we do not guarantee the same.</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">What is the application Process like?</h2>
                                <div class="ac-a">
                                    <p>The application process is completely online, and on approval, the cash is immediately transferred to the bank account of the user. </p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">How much can I borrow and for how long?</h2>
                                <div class="ac-a">
                                    <p>SureCredit offers loans ranging from ₹5, OOO– ₹1 Lakh, with terms between 65 to 90 days.</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">How much interest does it cost?</h2>
                                <div class="ac-a">
                                    <p>Typically, our interest rates start at 2.5% per month. Start an application here to see what rate you can borrow at.</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">Why do you need my bank account details?</h2>
                                <div class="ac-a">
                                    <p>We need your bank account details to transfer the approved loan amount.</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">Why do you need to verify my bank account?</h2>
                                <div class="ac-a">
                                    <p>We need to ensure that the bank account that you have provided belongs to you.</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">Is it possible to receive the loan amount in a joint bank account?</h2>
                                <div class="ac-a">
                                    <p>Yes, it is possible to get the loan amount transferred to a bank account jointly held by you. However, you should be the primary account holder of the joint account.</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">Can I get the loan amount transferred to someone else’s bank account?</h2>
                                <div class="ac-a">
                                    <p>No, you can only get the amount transferred to your own bank account.</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">My profile verification has been pending for more than 24 hours. Why?</h2>
                                <div class="ac-a">
                                    <p>Usually, the profile verification happens within 10 minutes of registration. However, in rare cases we may require more time to verify the profile. This may happen in case of low clarity documents, user provided details not matching with the documents uploaded, etc.</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">What if my app gets stuck down before completing the application? Do I have to restart the Process?</h2>
                                <div class="ac-a">
                                    <p>No, you just need to open our app and you can continue from where you left. In case of any other problems feel free to contact us at <a href="mailto:support@SureCredit.in">support@SureCredit.in</a></p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">How do I give access to SureCredit on my phone?</h2>
                                <div class="ac-a">
                                    <p>In your Android Phone, kindly go to Settings > Apps > SureCredit > Permissions and make sure that you have enabled SureCredit to access the application.</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">Where is the loan disbursed?</h2>
                                <div class="ac-a">
                                    <p>The loan amount is transferred directly to your bank account, which is provided by you during the loan application process.</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">Why do you need my permission to access my Phone?</h2>
                                <div class="ac-a">
                                    <p>SureCredit requires your permission to access your phone to complete the application process enabling us to provide seamless and efficient financial services.</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">Can I trust SureCredit with my data?</h2>
                                <div class="ac-a">
                                    <p>We use world-class data security and encryption techniques to protect the data you share with us. SureCredit never shares your information with third parties unless it is for dedicated business purposes, such as reporting defaulted loans to authorized credit bureaus. </p>
                                </div>
                            </div>

                               <div class="ac">
                                <h2 class="ac-q" tabindex="0">My application was not approved? What was the reason?</h2>
                                <div class="ac-a">
                                    <p>We decide the eligibility of every borrower based on our own set parameters and algorithms. We will let you know in case your application could not be approved.  </p>
                                </div>
                            </div>

                               <div class="ac">
                                <h2 class="ac-q" tabindex="0">How can I check my status of my loan application?</h2>
                                <div class="ac-a">
                                    <p>You can easily check the status of your application by logging into your account on https://www.SureCredit.in/ or the SureCredit App. </p>
                                </div>
                            </div>

                               <div class="ac">
                                <h2 class="ac-q" tabindex="0">My loan got disbursed, but the money is still not reflecting in my Bank account?</h2>
                                <div class="ac-a">
                                    <p>Once loan is disbursed, the money will reach your bank account within few hours. At the time of loan disbursal, the UTR (Unique Transaction Reference) number is shared with you, which you can track conveniently with your bank or please write to us at support@SureCredit.in attaching with screenshots. </p>
                                </div>
                            </div>

                               <div class="ac">
                                <h2 class="ac-q" tabindex="0">Is there a particular time at which I can apply for a loan through SureCredit?</h2>
                                <div class="ac-a">
                                    <p>No, you can avail a loan from SureCredit 24x7x365. Since we operate on a completely digitised platform, you can do it anywhere & anytime. </p>
                                </div>
                            </div>

                               <div class="ac">
                                <h2 class="ac-q" tabindex="0">How is SureCredit loan agreement signed?</h2>
                                <div class="ac-a">
                                    <p>You need to upload your e-signature in pdf, jpeg, format towards signing the loan agreement</p>
                                </div>
                            </div>

                               <div class="ac">
                                <h2 class="ac-q" tabindex="0">Why should I sign the loan agreement?</h2>
                                <div class="ac-a">
                                    <p>The signing makes the Loan Agreement is a contract which is a legally binding document and states that once a loan is disbursed to you, you are liable to pay back the entire amount with interest and other charges as applicable within the maturity of the loan. You cannot get a loan without signing the Loan Agreement. </p>
                                </div>
                            </div>

                               <div class="ac">
                                <h2 class="ac-q" tabindex="0">What if my loan is rejected?</h2>
                                <div class="ac-a">
                                    <p>Loan request are usually rejected if the details provided by you fail to meet our credit norms. In some cases, we do share the reason directly to you via SMS and mail and can only advice to reapply post resolving those issues. </p>
                                </div>
                            </div>

                               <div class="ac">
                                <h2 class="ac-q" tabindex="0">Will my loan be reported to CIBIL/other credit bureaus?</h2>
                                <div class="ac-a">
                                    <p>Yes, your loan and performance of your loan track will be reported to CIBIL, CRIF and other credit bureaus; as per the RBI guidelines.  </p>
                                </div>
                            </div>

                               <div class="ac">
                                <h2 class="ac-q" tabindex="0">Issues Uploading documents. What should I do?</h2>
                                <div class="ac-a">
                                    <h5 class="fsbold">Common Errors, Causes, and Solutions</h5>
                                     <ul class="listItems">
                                         <li>The document is corrupted</li>
                                         <li>Timeout during upload </li>
                                         <li>Document file size is too large</li>
                                         <li>File not in a given format</li>
                                         <li>Additionally, for bank statements are acceptable in pdf formats which you receive through mail sent by bank or which you can download from bank website directly.</li>
                                     </ul>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">How long does it typically take for me to complete my profile and avail a loan through SureCredit?</h2>
                                <div class="ac-a">
                                    <p>Completing your profile and uploading your documents should not take you more than 15 minutes. If all your documents are provided correctly, and your account is verified, you may receive the approved loan amount in your bank account within minutes.</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">How can I repay the loan</h2>
                                <div class="ac-a">
                                    <p>You can repay your loan by heading over to the Active loan section in the app and making a payment. We accept payments via a Gpay, Phonepe, UPI, NEFt.</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">Can I pre-pay my loan?</h2>
                                <div class="ac-a">
                                    <p>Yes, you have the option to prepay the loan amount earlier than the selected tenor.</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">Are there any charges for foreclosing my loan</h2>
                                <div class="ac-a">
                                    <p>No, you can foreclose your loan without any penalties.</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">Do you have a cash payment option?</h2>
                                <div class="ac-a">
                                    <p>No, As such we don’t have cash payment option.</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">What happens if I do not pay on time?</h2>
                                <div class="ac-a">
                                    <p>Timely repayment of the loan amount, interest and other applicable charges is extremely vital.</p>
                                    <ul class="listItems">
                                        <li>For every day that you default on your payment, you will have to pay a penalty charges </li>
                                        <li>You will automatically be ineligible for availing quicker Personal Loans of a higher value from SureCredit in the future.</li>
                                        <li>Your CIBIL score will get affected, which will affect your ability to avail loans from any bank or financial institution in the future.</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">Can I get my loan cancelled?</h2>
                                <div class="ac-a">
                                    <p>Once a loan is approved and disbursed, it cannot be cancelled. Hence, you would need to repay the amount in full in order to close the loan.</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">Can I make a partial payment on the due date and pay the remaining amount later?</h2>
                                <div class="ac-a">
                                    <p>No. There is no option for partial payment later after due date.</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">Do I get a confirmation after I repay the loan</h2>
                                <div class="ac-a">
                                    <p>Yes, you will receive an SMS and Email from SureCredit confirming the repaymentof your Personal Loan with SureCredit.</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">Money got deducted from my account, but it isn’t visible on my system. Why?</h2>
                                <div class="ac-a">
                                    <p>There can be times when, due to network issues or issues with the payment gateway, money gets debited from your account but it does not get accepted by the payment gateway. In these cases, you should get your money back within 7 working days. If you do not get the money back in your account by this time, please write to us at support@SureCredit.in attaching with screenshots.</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">Are there any hidden charges?</h2>
                                <div class="ac-a">
                                    <p>No. All our charges are transparent and communicated to the customer upfront</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">How easy to apply for another loan if I need fund again?</h2>
                                <div class="ac-a">
                                    <p>Once you are approved to join the SureCredit platform and successfully complete one loan cycle, future financing is literally at your fingertips. As and when you require funds, apply online within 10 minutes for new loans and receive funds directly in your bank account.</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">How do I check the details of my existing loan?</h2>
                                <div class="ac-a">
                                    <p>You can login to the SureCredit App and check your existing loan details under 'My Loans' section in the Main menu.</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">I have lost my job. How can I pay my Dues?</h2>
                                <div class="ac-a">
                                    <p>Please make alternate arrangements with help from family / friends to pay for dues on time. In case of any delay payment, please get in touch with relationship manager. Stay in touch & repay within the extended timeline as committed. Commitment should not cross the extended timeline. </p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">What are your Working Hours?</h2>
                                <div class="ac-a">
                                    <p>Our business hours are 9AM to 6PM IST, Monday to Friday with the exception of bank holidays. However, our team extend our services 24/7 for all loan disbursals are concerned.</p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">How Can I reach out to SureCredit?</h2>
                                <div class="ac-a">
                                    <p>For any questions or queries, you can reach out to us at <a href="mailto:support@SureCredit.in">support@SureCredit.in</a></p>
                                </div>
                            </div>

                            <div class="ac">
                                <h2 class="ac-q" tabindex="0">Is it safe to apply for SureCredit Online?</h2>
                                <div class="ac-a">
                                    <p>It is absolutely safe. All your sensitive information, which are personal, financial or related to employment are encrypted through the Secure Socket Layer (SSL) protocol. Your information is kept absolutely safe. Please refer to our Privacy Policy. We are legally compliant with all data privacy and IT security norms in India.</p>
                                </div>
                            </div>

                        </div> 
                        <!--/ accordion -->                     
                    </div>
                </div>
               <!--/ container -->             
            </div>
            <!--/ sub page Body Content-->
        </section>
        <!--/ subpage body -->
    </main>
    <!--/ main --> 
    <?php include 'includes/footer.php' ?> 
    <?php include 'includes/scripts.php'?>
</body>

</html>