<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sure Credit Contact</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/fav.png"> 
    <?php include 'includes/styles.php'?>
    <?php include 'includes/data.php'?>
</head>

<body> <?php include'includes/header.php' ?>
    <!-- main -->
    <main class="subpageMain">
        <!-- subpage header -->
        <section class="subpageHeader">
            <!-- container -->
            <div class="container">
                <article>
                    <h6 class="h6">Reach Us</h6>
                    <h1>Contact</h1>
                </article>
            </div>
            <!--/ container -->
        </section>
        <!--/ sub page header -->

        <!-- subpage body -->
        <section class="subpageBody">
            <!-- container -->
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>                        
                        <li class="breadcrumb-item active" aria-current="page">Contact</li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
            <!-- sub page Body Content -->
            <div class="subpageBodyContent">
               <!-- container -->
               <div class="container">
                   <p class="text-center fsbold">If you have any questions or queries a member of SureCredit will always be happy to help.</p>
                   <p class="text-center fsbold">Feel free to contact us by telephone or email and we will be sure to get back to you as soon as possible</p>
                <!-- row -->
                <div class="row py-3 py-lg-5 justify-content-center">
                    <!-- col -->
                    <div class="col-md-3 text-center">
                        <div class="colCard">
                            <div class="iconDiv mx-auto">
                                <span class="icon-telephone2 icomoon"></span>
                            </div>
                            <article class="py-3">
                                <h5 class="h5">Call us</h5>
                                <p class="mb-0 pb-1">+91 9642123255</p>                                
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-md-3 text-center">
                        <div class="colCard">
                            <div class="iconDiv mx-auto">
                                <span class="icon-email01 icomoon"></span>
                            </div>
                            <article class="py-3">
                                <h5 class="h5">Email Us</h5>
                                <p class="mb-0 pb-1">info@surecredit.in</p>
                                <p>support@surecredit.in</p>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-md-3 text-center d-none">
                        <div class="colCard">
                            <div class="iconDiv mx-auto">
                                <span class="icon-location02 icomoon"></span>
                            </div>
                            <article class="py-3">
                                <h5 class="h5">Visit Us</h5>
                                <p>Plot No:91, Viajaya Colony, Jublihills, Hyderabad, Telangana - 500072</p>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-md-3 text-center d-none">
                        <div class="colCard">
                            <div class="iconDiv mx-auto">
                                <span class="icon-arrows icomoon"></span>
                            </div>
                            <article class="py-3">
                                <h5 class="h5">Fax</h5>
                                <p class="mb-0 pb-1">+91 9642123255</p>
                                <p>+1200 800 800</p>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->

                <!-- row -->
                <div class="row py-2 py-lg-5">
                    <div class="col-md-6 align-self-center">
                        <h2 class="h2 fbold">Get in Touch with Us</h2>
                        <p class="mb-0">Our customer service agents are available from </p>
                        <p class="fbold">Monday to Saturday: 9:00am – 6:00pm (IST)</p>

                        <form class="contactForm">
                            <div class="row">
                                <!-- col -->
                                <div class="col-md-12">
                                    <div class="form-floating mb-3">
                                        <input type="text" class="form-control" id="name" placeholder="Your Name">
                                        <label for="name">Write Your Name</label>
                                    </div>
                                </div>
                                <!--/ col -->
                                <!-- col -->
                                <div class="col-md-6">
                                    <div class="form-floating mb-3">
                                        <input type="text" class="form-control" id="phone" placeholder="Phone Number">
                                        <label for="phone">Phone Number</label>
                                    </div>
                                </div>
                                <!--/ col -->
                                <!-- col -->
                                <div class="col-md-6">
                                    <div class="form-floating mb-3">
                                        <input type="text" class="form-control" id="email" placeholder="Email Address">
                                        <label for="email">Email Address</label>
                                    </div>
                                </div>
                                <!--/ col -->
                                <!-- col -->
                                <div class="col-md-12">
                                    <div class="form-floating mb-3">
                                        <!-- <input type="text" class="form-control" id="name" placeholder="Your Name"> -->
                                        <textarea class="form-control" style="height:85px"; placeholder="Wriet Message" id="msg"></textarea>
                                        <label for="msg">Write Your Name</label>
                                    </div>
                                </div>
                                <!--/ col -->
                                 <!-- col -->
                                <div class="col-md-12">
                                     <button class="w-100">Submit Enquiry</button>
                                </div>
                                <!--/ col -->
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <img src="img/contactsend.svg" class="img-fluid" alt="">
                    </div>
                </div>
                <!--/ row -->
               </div>
               <!--/ container -->             
            </div>
            <!--/ sub page Body Content-->
        </section>
        <!--/ subpage body -->
    </main>
    <!--/ main --> 
    <?php include 'includes/footer.php' ?> 
    <?php include 'includes/scripts.php'?>
</body>

</html>