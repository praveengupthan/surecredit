<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sure Credit</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/fav.png"> 
    <?php include 'includes/styles.php'?>
    <?php include 'includes/data.php'?>
</head>

<body> <?php include'includes/headerPostLogin.php' ?>
    <!-- main -->
    <main class="subpageMain profileSubPage">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <?php include 'includes/userLeftNav.php'?>
                <!-- right section -->
                <div class="col-lg-8">
                    <div class="shadowBox">
                        <!-- content -->
                        <div class="profileContent p-2 p-lg-4">
                           
                            <div class="row">
                                <div class="col-md-4">
                                    <h4 class="text-center">Scan and Pay</h4>
                                    <img src="img/qrcode.png" alt="" class="img-fluid">
                                </div>
                                <div class="col-md-8 border-md-left align-self-center">
                                     <article class="pb-4">
                                         <h5 class="fbold">Pay with UPI</h5>
                                         <p>praveenguptha.n@icici</p>
                                     </article>

                                      <article>
                                         <h5 class="fbold">Pay with Bank Details</h5>
                                         <p class="pb-0 mb-0">Bank Name: <span class="fbold">State Bank of India</span></p>
                                         <p class="pb-0 mb-0">Account Number: <span class="fbold">01225689</span></p>
                                         <p class="pb-0 mb-0">Branch Name: <span class="fbold">Kukatpally Branch</span></p>
                                     </article>
                                </div>
                            </div>
                        

                           
                                         
                           
                        </div>
                        <!--/ content -->
                    </div>
                </div>
                <!--/ right section -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->

    </main>
    <!--/ main --> 
    <?php include 'includes/footer.php' ?> 
    <?php include 'includes/scripts.php'?>
</body>

</html>