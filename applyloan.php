<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sure Credit Privacy</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/fav.png"> 
    <?php include 'includes/styles.php'?>
    <?php include 'includes/data.php'?>
</head>

<body> <?php include'includes/header.php' ?>
    <!-- main -->
    <main class="subpageMain">
        <!-- subpage body -->
        <section class="subpageBody">           
            <!-- sub page Body Content -->
            <div class="applypage">
                <!-- container -->
                <div class="container">
                    <p>I Wish to pay</p>
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-4">
                            <div class="form-check form-check-inline border p-3 d-block">
                                <input class="form-check-input ms-2 me-3" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                <label class="form-check-label" for="inlineRadio1">EMI for 95 Days (3 Months & 5 days) </label>
                            </div>
                        </div>
                        <!--/ col -->
                         <!-- col -->
                        <div class="col-lg-4">
                            <div class="form-check form-check-inline border p-3 d-block">
                                <input class="form-check-input ms-2 me-3" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                <label class="form-check-label" for="inlineRadio2">EMI for 65 Days (2 Months & 5 days) </label>
                            </div>
                        </div>
                        <!--/ col -->
                         <!-- col -->
                        <div class="col-lg-4">
                            <div class="form-check form-check-inline border p-3 d-block">
                                <div class="d-flex">
                                     <input class="form-check-input ms-2 me-3" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3">
                                    <label class="form-check-label me-3" for="inlineRadio3">Enter the Days </label>
                                    <input type="text" class="w-50 m-0 align-self-center">
                                </div>
                            </div>
                        </div>
                        <!--/ col -->
                    </div>

                     <!-- ranger -->
                     <div class="ranger">
                         <p>Drag the slider to display the current value.</p>
                        <div class="slidecontainer py-3">
                            <input type="range" min="10000" max="2500000" value="50" class="slider" id="myRange" step="10000">
                            <p class="d-flex justify-content-between pt-3"><span>Loan Amount:</span> <span>Rs: <span id="demo" class="fbold"></span>.00</span></p>
                        </div>
                     </div>
                     <!--/ ranger -->

                     <div class="priceItem py-2 mb-2 border-bottom">
                         <p class="d-flex justify-content-between">
                            <span>Total Loan Amount</span>
                            <span>Rs: 10,000.00</span>
                         </p>
                     </div>

                     <div class="priceItem py-2 mb-2 border-bottom">
                         <p class="d-flex justify-content-between">
                            <span>Payble  Amount</span>
                            <span>Rs: 10,250.00</span>
                         </p>
                     </div>

                     <div class="priceItem py-2 mb-2 border-bottom">
                         <p class="d-flex justify-content-between">
                            <span>You will pay Extra Amount</span>
                            <span>Rs: 2,250.00</span>
                         </p>
                     </div>

                      <div class="priceItem py-2 mb-2 border-bottom">
                         <p class="d-flex justify-content-between">
                            <span>Bank Interest Rate</span>
                            <span>8.5%</span>
                         </p>
                     </div>

                      <div class="priceItem py-2 mb-2 border-bottom">
                         <p class="d-flex justify-content-between">
                            <span>Processing Fee</span>
                            <span>1.5%</span>
                         </p>
                     </div>

                     <div class="row justify-content-center py-3">
                         <div class="col-md-4 text-center">
                                <div class="d-flex">
                                    <div>
                                        <h2 class="h2 fsbold mb-0 pb-0">EMI</h2>
                                        <small>Per month</small>
                                    </div>
                                    <div class="ps-4">
                                        <h1 class="h1 fbold">Rs: 2,882</h1>                                        
                                    </div>
                                </div>
                         </div>
                     </div>

                     <p>I am Agree with</p>
                     <div class="acceptterms pb-3">
                         <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                            <label class="form-check-label" for="inlineCheckbox1">Terms &amp; Conditions</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
                            <label class="form-check-label" for="inlineCheckbox2">Privacy Policy</label>
                        </div>
                     </div>

                     <button class="btnCustom">Apply Now</button>


                </div>
                <!--/ container -->
            </div>
            <!--/ sub page Body Content-->
        </section>
        <!--/ subpage body -->
    </main>
    <!--/ main --> 
    <?php include 'includes/footer.php' ?> 
    <?php include 'includes/scripts.php'?>

    <script>
    var slider = document.getElementById("myRange");
    var output = document.getElementById("demo");
    output.innerHTML = slider.value;

    slider.oninput = function() {
    output.innerHTML = this.value;
    }
    </script>

</body>

</html>