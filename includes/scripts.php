<!-- scripts -->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/masterslider.min.js"></script>
    <script src="js/bsnav.min.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/swiper.min.js"></script>
    <script src="js/accordion.min.js"></script>
    <script>
        var accordion = new Accordion('.accordion-container');
        var accordion = new Accordion('.accordion-container2');
        var accordion = new Accordion('.accordion-container3');

    </script>
    <!--[if IE lt 9]>
                <script src="js/html5-shiv.js"></script>
            <[if ends ]-->
    <!--/ script files -->
    <script src="js/custom.js"></script>
    <script>
        //contact form validatin
        $('#contact_form').validate({
            ignore: [],
            errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
            errorElement: 'div',
            errorPlacement: function (error, e) {
                e.parents('.form-floating').append(error);
            },
            highlight: function (e) {
                $(e).closest('.form-floating').removeClass('has-success has-error').addClass('has-error');
                $(e).closest('.text-danger').remove();
            },
            rules: {
                name: {
                    required: true
                },
                phone: {
                    required: true,
                    number: true,
                    minlength: 10,
                    maxlength: 10
                },
                email: {
                    required: true,
                    email: true
                },
                sub: {
                    required: true,
                }
            },

            messages: {
                name: {
                    required: "Enter Name"
                },
                phone: {
                    required: "Enter Valid Mobile Number"
                },
                email: {
                    required: "Enter Valid Email"
                },

                sub: {
                    required: "Enter Subject"
                }
            },
        });
    </script>
    <script>
        var masterslider = new MasterSlider();

        // slider controls
        masterslider.control('bullets', { autohide: true, overVideo: true, dir: 'v', align: 'right', space: 6, margin: 30 });
        // slider setup
        masterslider.setup("masterslider", {
            width: 1366,
            height: 500,
            minHeight: 0,
            space: 0,
            start: 1,
            grabCursor: true,
            swipe: true,
            mouse: true,
            keyboard: true,
            layout: "fullwidth",
            wheel: true,
            autoplay: true,
            instantStartLayers: true,
            loop: true,
            shuffle: false,
            preload: 0,
            heightLimit: true,
            autoHeight: false,
            smoothHeight: true,
            endPause: false,
            overPause: true,
            fillMode: "fill",
            centerControls: true,
            startOnAppear: true,
            layersMode: "center",
            autofillTarget: "",
            hideLayers: false,
            fullscreenMargin: 0,
            speed: 20,
            dir: "v",
            parallaxMode: 'swipe',
            view: "basic"
        });

    </script>
    <!--/ scripts -->