<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sure Credit</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/fav.png"> 
    <?php include 'includes/styles.php'?>
    <?php include 'includes/data.php'?>
</head>

<body> <?php include'includes/headerPostLogin.php' ?>
    <!-- main -->
    <main class="subpageMain profileSubPage">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <?php include 'includes/userLeftNav.php'?>
                <!-- right section -->
                <div class="col-lg-8">
                    <div class="shadowBox">
                        <!-- content -->
                        <div class="profileContent p-2 p-lg-4">
                            <!-- row -->
                            <div class="row mb-4">
                                <div class="col-md-12">
                                    <h5 class="flight h5 border-bottom mb-3 pb-3"><span class="fbold fblue">Repay Loan</span></h5>
                                </div>                              
                            </div>
                            <!--/ row -->
                            <div class="d-md-flex justify-content-md-between">
                                <div class="form-floating repaySelect">
                                    <select class="form-select" id="selectAccount" aria-label="Floating label select example">
                                        <option selected>Select Account</option>
                                        <option value="1">Account 1</option>
                                        <option value="2">Account 2</option>
                                        <option value="3">Account 3</option>
                                    </select>
                                    <label for="selectAccount">Select Account</label>
                                </div>
                                <a href="javascript:void(0)" class="btnCustom d-inline-block">Pay 12,000.00</a>
                            </div>

                            <!-- table -->
                            <div class="table-responsive customTable">
                                <table class="table table-striped table-hover repayloan">
                                    <thead>
                                        <tr>
                                        <th scope="col">Select Month</th>
                                        <th scope="col">EMI Amount</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Due Date</th>
                                        <th scope="col">Receipt</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>                                          
                                            <td><input type="checkbox"><span class="d-inline-block ps-2">January 2022</span></td>
                                            <td>Rs: 5455.00</td>
                                            <td><span class="fgreen">Paid</span></td>
                                            <td>05-01-2022</td>
                                             <td>
                                                 <a href="javascript:void(0)"><span class="icon-download01 icomoon"></span></a>
                                             </td>
                                        </tr>
                                         <tr>                                          
                                            <td><input type="checkbox"><span class="d-inline-block ps-2">February 2022</span></td>
                                            <td>Rs: 5455.00</td>
                                            <td>Pending</td>
                                            <td>05-01-2022</td>
                                             <td>
                                                 <a href="javascript:void(0)"><span class="icon-download01 icomoon"></span></a>
                                             </td>
                                        </tr>
                                        <tr>                                          
                                            <td><input type="checkbox"><span class="d-inline-block ps-2">March 2022</span></td>
                                            <td>Rs: 5455.00</td>
                                            <td>Pending</td>
                                            <td>05-01-2022</td>
                                             <td>
                                                 <a href="javascript:void(0)"><span class="icon-download01 icomoon"></span></a>
                                             </td>
                                        </tr>
                                        <tr>                                          
                                            <td><input type="checkbox"><span class="d-inline-block ps-2">April 2022</span></td>
                                            <td>Rs: 5455.00</td>
                                            <td>Pending</td>
                                            <td>05-01-2022</td>
                                             <td>
                                                 <a href="javascript:void(0)"><span class="icon-download01 icomoon"></span></a>
                                             </td>
                                        </tr>
                                        <tr>                                          
                                            <td><input type="checkbox"><span class="d-inline-block ps-2">May 2022</span></td>
                                            <td>Rs: 5455.00</td>
                                            <td>Pending</td>
                                            <td>05-01-2022</td>
                                             <td>
                                                 <a href="javascript:void(0)"><span class="icon-download01 icomoon"></span></a>
                                             </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!--/ table -->

                           
                                         
                           
                        </div>
                        <!--/ content -->
                    </div>
                </div>
                <!--/ right section -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->

    </main>
    <!--/ main --> 
    <?php include 'includes/footer.php' ?> 
    <?php include 'includes/scripts.php'?>
</body>

</html>