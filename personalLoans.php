<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sure Credit Personal Loans</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/fav.png"> 
    <?php include 'includes/styles.php'?>
    <?php include 'includes/data.php'?>
</head>

<body> <?php include'includes/header.php' ?>
    <!-- main -->
    <main class="subpageMain">
        <!-- subpage header -->
        <section class="subpageHeader">
            <!-- container -->
            <div class="container">
                <article>
                    <h6 class="h6">Services</h6>
                    <h1>Personal Loans</h1>
                </article>
            </div>
            <!--/ container -->
        </section>
        <!--/ sub page header -->

        <!-- subpage body -->
        <section class="subpageBody">
            <!-- container -->
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>                        
                        <li class="breadcrumb-item active" aria-current="page">Personal Loans</li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
            <!-- sub page Body Content -->
            <div class="subpageBodyContent">
                <!-- container  -->
                <div class="container">
                     <div class="row justify-content-center">
                         <div class="col-md-8 text-center">
                            <h2 class="h2 fbold">So, how does Personal Loan work?</h2>
                            <p>We understand your emergency and therefore, have made Personal Loan available few clicks away. Besides quick disbursal, we also ensure higher loan amount with easy repayment options and desired flexibility. Check your eligibility for a Personal Loan online and instantly by getting started.</p>
                         </div>
                     </div>
                 </div>
                 <!--/ container -->
                 <figure class="mt-3">
                     <img src="img/personalloanbanner.jpg" alt="" class="img-fluid">
                 </figure>

                 <!-- container -->
                <div class="bannercontent">
                    <div class="container text-center">
                        <div class="row justify-content-center py-3 py-lg-3">
                            <div class="col-md-8">
                                <h2 class="fbold h2">Already know your Personal Loan amount and want instant, easy and online process?</h2>
                                <p class="py-2 py-lg-3 mb-0">At LoanTap, we believe in fast online disbursal of Personal Loans and you will be happy to see outstanding loan reducing every month.</p>
                                <a href="javascript:void(0)">Apply Loan</a>
                            </div>
                        </div>
                    </div>
                </div>
                 <!--/ container -->
               
                   <!-- container -->
                <div class="container">
                    <div class="row justify-content-center py-2 py-lg-5">
                         <div class="col-md-10 text-center">
                            <h2 class="h2 fbold">Choose the Right Loan</h2>     
                            <h6></h6>                      
                         </div>
                     </div>
                     <!-- row -->
                     <div class="row py-2">
                         <div class="col-md-6">
                             <img src="img/medicalloanimg.jpg" alt="" class="img-fluid">
                         </div>
                         <div class="col-md-6 align-self-center">                           
                            <h4 class="h4 fbold">Medical Loan</h4>
                            <p>Medical emergencies can be extremely frightening, especially if you are unprepared! While many people have started investing in medical insurance, a large number of people are still uncovered, especially for emergencies. And at times, even insurance will not cover certain medical expenses. </p>
                            <p>In times like these, Our Instant loan would help during these medical emergencies, no matter what the situation might be! This will be instant and hassle-free so that you don’t have to worry about finances during an emergency. </p>                           
                         </div>
                     </div>
                     <!--/ row -->

                     <!-- row -->
                     <div class="row py-2">
                         <div class="col-md-6 order-md-last">
                             <img src="img/travelloan.jpg" alt="" class="img-fluid">
                         </div>
                         <div class="col-md-6 align-self-center">                           
                            <h4 class="h4 fbold">Travel Loan</h4>
                            <p>At SureCredit, we understand how busy your daily routine is, take a break. Some travel for fun, others for relaxation, while some for adventure. And with SureCredit’s line of credit that can be used as a travel loan, all you need to do is decide on your destination, and off you go!  </p>                           
                         </div>
                     </div>
                     <!--/ row -->

                      <!-- row -->
                     <div class="row py-2">
                         <div class="col-md-6">
                             <img src="img/emibills.jpg" alt="" class="img-fluid">
                         </div>
                         <div class="col-md-6 align-self-center">                           
                            <h4 class="h4 fbold">Shell out your EMI bills</h4>
                            <p>It is not an easy task to maintain all your financial obligations just with your salary. Apply for our instant loan, get it verified without any physical documents using our online app and pay all your EMI bills and house rent  </p>                           
                         </div>
                     </div>
                     <!--/ row -->

                     <!-- row -->
                     <div class="row py-2">
                         <div class="col-md-6 order-md-last">
                             <img src="img/homerenovation.jpg" alt="" class="img-fluid">
                         </div>
                         <div class="col-md-6 align-self-center">                           
                            <h4 class="h4 fbold">Home Renovation Loan</h4>
                            <p>Home is where happiness is. Give your home a new look or carry out the vital repairs. What will you do in such a situation? No need to think further. With SureCredit you can get approved for a line of credit. We provide loans up to Rs 1,50,000 by just applying for an instant personal loan and renovate your sweet home in just the way you dream it. </p>                           
                         </div>
                     </div>
                     <!--/ row -->

                     <!-- row -->
                     <div class="row py-2">
                         <div class="col-md-6">
                             <img src="img/ConsiderationLoan.jpg" alt="" class="img-fluid">
                         </div>
                         <div class="col-md-6 align-self-center">                           
                            <h4 class="h4 fbold">Debt Consideration Loan</h4>
                            <p>Consolidate your multiple EMIs into a single EMI and pay off your debt with a personal loan from SureCredit. A debt consolidation loan helps you in getting rid of your debt faster and saving money on interest. </p>                           
                         </div>
                     </div>
                     <!--/ row -->
                </div>
                <!--/ containe r-->
            </div>
            <!--/ sub page Body Content-->
        </section>
        <!--/ subpage body -->
    </main>
    <!--/ main --> 
    <?php include 'includes/footer.php' ?> 
    <?php include 'includes/scripts.php'?>
</body>

</html>