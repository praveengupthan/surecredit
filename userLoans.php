<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sure Credit</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/fav.png"> 
    <?php include 'includes/styles.php'?>
    <?php include 'includes/data.php'?>
</head>

<body> <?php include'includes/headerPostLogin.php' ?>
    <!-- main -->
    <main class="subpageMain profileSubPage">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <?php include 'includes/userLeftNav.php'?>
                <!-- right section -->
                <div class="col-lg-8">
                    <div class="shadowBox">
                        <!-- content -->
                        <div class="profileContent p-2 p-lg-4">
                            <!-- row -->
                            <div class="row mb-4">
                                <div class="col-md-12">
                                    <h5 class="flight h5 border-bottom mb-3 pb-3"><span class="fbold fblue">My Loans</span></h5>
                                </div>                              
                            </div>
                            <!--/ row -->
                            <!-- row -->
                            <div class="row">
                                <!-- col -->
                                <?php 
                                for ($i=0; $i<count($loanItem); $i++) {?>
                                <div class="col-md-6">
                                    <div class="loanItemCard">
                                        <div class="d-flex justify-content-between pb-2 border-bottom">
                                            <h6 class="h6 fbold text-uppercase"><?php echo $loanItem [$i][0]?></h5>
                                            <a href="<?php echo $loanItem [$i][1]?>" class="fsbold fgreen">View Details</a>
                                        </div>
                                        <div class="d-flex justify-content-between py-4 border-bottom">
                                              <div>
                                                  <p class="fgray mb-1"><small><?php echo $loanItem [$i][2]?></small></p>
                                                  <h6 class="fbold h6"><?php echo $loanItem [$i][3]?></h6>
                                              </div>
                                               <div>
                                                  <p class="fgray mb-1"><small><?php echo $loanItem [$i][4]?></small></p>
                                                   <h6 class="fbold h6"><?php echo $loanItem [$i][5]?></h6>
                                              </div>
                                        </div>
                                        <div class="d-flex justify-content-between">
                                            <p class="small mb-0 pt-2"><span class="fgray"><?php echo $loanItem [$i][6]?></span>:<span class="fbold d-block"> <?php echo $loanItem [$i][7]?></span></p>

                                            <p class="small mb-0 pt-2"><span class="fgray"><?php echo $loanItem [$i][8]?></span>:<span class="fbold d-block"> <?php echo $loanItem [$i][9]?></span></p>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                                <!--/ col -->    
                            </div>
                            <!--/ row -->
                        </div>
                        <!--/ content -->
                    </div>
                </div>
                <!--/ right section -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->

    </main>
    <!--/ main --> 
    <?php include 'includes/footer.php' ?> 
    <?php include 'includes/scripts.php'?>
</body>

</html>