<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sure Credit How it Works</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/fav.png"> 
    <?php include 'includes/styles.php'?>
    <?php include 'includes/data.php'?>
</head>

<body> <?php include'includes/header.php' ?>
    <!-- main -->
    <main class="subpageMain">
        <!-- subpage header -->
        <section class="subpageHeader">
            <!-- container -->
            <div class="container">
                <article>
                    <h6 class="h6">Process</h6>
                    <h1>How it Works</h1>
                </article>
            </div>
            <!--/ container -->
        </section>
        <!--/ sub page header -->

        <!-- subpage body -->
        <section class="subpageBody">
            <!-- container -->
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>                        
                        <li class="breadcrumb-item active" aria-current="page">How it Works</li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
            <!-- sub page Body Content -->
            <div class="subpageBodyContent">
                <!-- container -->
                <div class="container">
                     <div class="row py-2 py-lg-5">
                        <div class="col-md-6">
                            <img src="img/mission.svg" alt="" class="img-fluid">
                        </div>
                        <div class="col-md-6 align-self-center">
                            <h2 class="fbold fblue">We Works for You</h2>
                            <p>If you need to meet your urgent needs, SureCredit is a one stop solution for all your instant cash needs. We at SureCredit provide you with instant personal loan approvals to fund all your financial requirements within the short span of minutes! You can simply download the app and apply for the loan anytime anywhere in India. We here have different loans based on your needs. At SureCredit, our finance experts are available to answer your questions. We constantly work towards making your process easier. </p>
                            <h5 class="fbold fblue h5">Availing of Instant Personal Loan is Simple, & hassle-free with Simple Steps</h5>
                            <p>If you are looking for an instant personal loan? we are here. Reliable and instant with minimal documentation. Get money credited into your bank account within span of time. You can get personal loans that are quick and easy in approval without physical verification and documents as well- Moreover, the process has become digital; thus, reducing time in the verification process. Application Process</p>
                        </div>
                     </div>
                </div>
                <!--/ container -->
               <!-- process -->
                <section class="homeProcess py-3 py-lg-5">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-md-6 text-center">
                                <h2 class="sectionTitle">Instant Cash on the go!</h2>
                                <p class="pt-3">Borrow and Repay on your own terms. Effortless application. Takes 5 mins to apply.
        Get instant cash transferred to your bank account in minutes. </p>
                            </div>
                        </div>
                        <!-- row -->
                        <div class="row pt-2 pt-lg-4 justify-content-center position-relative instantrow">
                            <!-- col -->
                            <?php 
                            for ($i=0; $i<count($processItem); $i++){ ?>
                            <div class="col-md-2 text-center instantcol text-center">
                                <div class="instantCircle">
                                    <span class="<?php echo $processItem[$i][0]?> icomoon"></span>
                                    <span class="number"><?php echo $processItem[$i][1]?></span>
                                </div>
                                <article class="pt-4">
                                    <h5 class="h5 pt-2"><?php echo $processItem[$i][2]?></h5>
                                    <p><?php echo $processItem[$i][3]?></p>
                                </article>
                            </div>
                            <?php } ?>
                            <!--/ col -->                   
                        </div>
                        <!--/ row -->
                    </div>
                </section>
                <!--/ process -->

            <section class="py-2 py-lg-5">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <img src="img/howitworks01.svg" class="img-fluid" alt="">
                        </div>
                            <div class="col-md-6 align-self-center">
                            <h2 class="h2 fbold">Now Get an Instant Personal Loan within Minutes with a simple registration process</h2>
                                <ul class="listItems pt-2 pt-lg-3">
                                    <li>Download SureCredit Instant loan app from Google Play Store</li>
                                    <li>Verify your mobile number through OTP</li>
                                    <li>Sign up using your Facebook or Google Account </li>
                                    <li>Check Eligibility by filling simple registration form</li>
                                    <li>Upload KYC documents (PAN, Address Proof)</li>
                                    <li>Upload Salary Proof & Salary reflecting Bank Statement</li>
                                    <li>Get your loan disbursed in minutes to Your Bank Account—subject to verification</li>
                                    <li>Keep track all your transactions</li>
                                </ul>
                        </div>
                    </div>

                     <div class="row pt-3 pt-lg-5">
                            <div class="col-md-6 order-lg-last">
                                <img src="img/personalloanimg.jpg" class="img-fluid" alt="">
                            </div>
                            <div class="col-md-6 rtContent align-self-center">
                                <h2 class="h2 fbold">Eligibility Criteria </h2>
                                <p>To make borrowing easy & accessible our eligibility parameters are simple</p>
                                <p class=""fbold>For salaried employees:</p>
                                <ul class="listItems pb-4">
                                    <li>Resident citizen of India</li>
                                    <li>Age limit is from 21 years up to 58 years</li>
                                    <li>Minimum monthly income required is ₹25,000 and above</li>
                                    <li>Documents handy (Pan Card, Address Proof, Salary Slip, Valid Bank Statement)</li>                                   
                                </ul>
                                <a href="javascript:void(0)" class="btnCustom">Get started</a>
                            </div>
                        </div>

                        <div class="row justify-content-center pt-2 pt-lg-5">
                                <div class="col-md-6 text-center">
                                    <h2 class="sectionTitle">Made in India</h2>
                                    <p class="pt-3">SureCredit is made in India and made for India. </p>
                                </div>
                        </div>

                        <!-- row -->
                        <div class="row py-3">
                            <!-- col -->
                            <?php 
                            for($i=0; $i<count($howItem); $i++) { ?>
                            <div class="col-md-3 text-center">
                                <div class="colCard">
                                    <div class="iconDiv mx-auto">
                                        <span class="<?php echo $howItem[$i][0]?> icomoon"></span>
                                    </div>
                                    <article class="py-3">
                                        <h5 class="h5"><?php echo $howItem[$i][1]?></h5>
                                        <p><?php echo $howItem[$i][2]?></p>
                                    </article>
                                </div>
                            </div>
                            <?php } ?>
                            <!--/ col -->                          
                    </div>
                    <!--/ row -->
                </div>
            </section>               
            </div>
            <!--/ sub page Body Content-->
        </section>
        <!--/ subpage body -->
    </main>
    <!--/ main --> 
    <?php include 'includes/footer.php' ?> 
    <?php include 'includes/scripts.php'?>
</body>

</html>