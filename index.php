<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sure Credit</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/fav.png">
    <?php include 'includes/styles.php'?>
    <?php include 'includes/data.php'?>
     
</head>

<body>
   
   <?php include'includes/header.php' ?>
    <!-- main -->
    <main>
        <?php include 'includes/slider.php'?>
        <!-- process -->
        <section class="homeProcess py-3 py-lg-5">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-lg-6 text-center">
                        <h2 class="sectionTitle">Instant Cash on the go!</h2>
                        <p class="pt-3">Borrow and Repay on your own terms. Effortless application. Takes 5 mins to apply.
Get instant cash transferred to your bank account in minutes. </p>
                    </div>
                </div>
                <!-- row -->
                <div class="row pt-2 pt-lg-4 justify-content-center position-relative instantrow">
                    <!-- col -->
                    <?php 
                    for ($i=0; $i<count($processItem); $i++){ ?>
                    <div class="col-lg-2 text-center instantcol text-center">
                        <div class="instantCircle">
                            <span class="<?php echo $processItem[$i][0]?> icomoon"></span>
                            <span class="number"><?php echo $processItem[$i][1]?></span>
                        </div>
                        <article class="pt-4">
                            <h5 class="h5 pt-2"><?php echo $processItem[$i][2]?></h5>
                            <p><?php echo $processItem[$i][3]?></p>
                        </article>
                    </div>
                    <?php } ?>
                    <!--/ col -->                   
                </div>
                <!--/ row -->
            </div>
        </section>
        <!--/ process -->
        <!-- why choose us -->
        <section class="whyChooseUs py-3 py-lg-5">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row justify-content-center">
                    <div class="col-lg-8 text-center align-self-center">
                        <h6 class="h6 fblue fbold">Features and Benefits</h6>
                        <h2 class="sectionTitle">Why choose Sure Credit</h2>                        
                        <p class="pt-3"> Our mission is to be a valued and trusted partner to the small and midsize business community. At SureCredit, we seek to provide a truly unique experience that sets us apart from other wealth management platforms. We believe there are several reasons why our platform stands out from the rest. We utilize advanced, comprehensive financial planning software to better serve you. And Don’t worry about providing collateral, risking any valuable asset, or our experienced professionals taking care of extensive documentation for personal loan at SureCredit. We take care of all these for you and provide you with a hassle-free process to take a personal loan online. So, what are you waiting for? Just click and Apply online. </p>
                    </div>
                     <div class="col-lg-4 align-self-center">
                        <img src="img/whychoosertimg.svg" alt="" class="img-fluid">
                    </div>
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
            <!-- container fluid -->
            <div class="container">
                <!-- row -->   
                <ul class="whyListItems row ">
                    <!-- item -->
                    <?php 
                    for($i=0; $i<count($whychooseItem); $i++) {?>
                        <div class="col-lg-4 col-md-6">
                        <div class="d-flex whyListItem">
                            <div class="iconDiv text-center">
                                <span class="<?php echo $whychooseItem [$i][0]?> icomoon"></span>
                            </div>
                            <article class="ps-2 ps-lg-4 align-self-center">
                                <h6 class="h6"><?php echo $whychooseItem [$i][1]?></h6>
                            </article>
                        </div>
                        </div>
                    <?php } ?>
                    <!-- item -->                          
                </ul>
                <!--/ row -->
            </div>
            <!--/ container fluid -->
        </section>
        <!--/ why choose us-->
        <!-- about -->
        <section class="aboutSection py-3 py-lg-5">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-md-6">
                        <figure class="aboutHomeImg">
                            <img src="img/aboutHome.png" alt="" class="img-fluid">
                        </figure>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-md-6 align-self-center">
                        <h2 class="sectionTitle">Instant Personal Loan Online</h2>
                        <h6 class="h6 fsbold pt-4 pb-2">You can now avail personal loan at SureCredit completely online without any physical documentation and verification. </h6>
                        <p>We at SureCredit provide you with instant personal loan approvals to fund all your financial requirements within the short span of minutes! Be it a trip to your dream destination, your dream wedding plan or even a medical emergency, a Personal Loan will always help you fulfil your requirements. When you apply for a Personal Loan online on SureCredit, we connect you with the best lending partners. </p>
                        <ul class="listItems row">
                            <li class="col-12 mb-2 mb-lg-4">Avail a Loan of up to Rs. 1.50 Lakhs</li>
                            <li class="col-12 mb-2 mb-lg-4">Affordable rate of interest</li>
                            <li class="col-12 mb-2 mb-lg-4">No hidden charges</li>                           
                        </ul>
                        <a href="about.php" class="aboutBtn">Read More</a>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </section>
        <!--/ about -->

        <!-- products -->
        <section class="loanProducts py-3 py-lg-5">
            <!-- container -->
            <div class="container">
                 <!-- row -->
                <div class="row justify-content-center">
                    <div class="col-md-12 text-center">
                        <h2 class="sectionTitle">Products</h2>
                        <p class="pt-3"> Our Loan Products Especiall for Professionals </p>
                        <p><a href="javascript:void(0)" class="btnCustom">More Products</a></p>
                    </div>
                </div>
                <!--/ row -->
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <?php 
                    for($i=0; $i<count($productItem); $i++) {?>
                    <div class="col-lg-4 mb-3 mb-lg-0">
                        <div class="card productCard">
                            <img src="img/<?php echo $productItem[$i][0]?>" class="card-img-top img-fluid" alt="...">
                            <div class="card-body">
                                <h5 class="h5 fbold card-title"><?php echo $productItem[$i][1]?></h5>
                                <p class="card-text"><?php echo $productItem[$i][2]?> </p>                                
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </section>
        <!--/ products -->

        <!-- mobile app section -->
        <section class="mobileapp">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <img src="img/getappimg.png" alt="" class="img-fluid">
                    </div>
                    <div class="col-md-7 text-center align-self-center">
                        <h2 class="h2 fbold">Get the app now</h2>
                        <p class="pt-3"> If you are looking for instant cash loan to meet your urgent needs,
We are here! Get Loan without physical verification and without document in 1 hour with our AI-based system </p>
                        <a href="javascript:void(0)">
                            <img src="img/googleplayimg.png" alt="" class="appimgand">
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <!--/ mobile app section -->
    </main>
    <!--/ main -->
    <?php include 'includes/footer.php' ?>  
    <?php include 'includes/scripts.php'?>
</body>

</html>