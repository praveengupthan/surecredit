<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sure Credit</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/fav.png"> 
    <?php include 'includes/styles.php'?>
    <?php include 'includes/data.php'?>
</head>

<body> <?php include'includes/headerPostLogin.php' ?>
    <!-- main -->
    <main class="subpageMain profileSubPage">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <?php include 'includes/userLeftNav.php'?>
                <!-- right section -->
                <div class="col-md-8">
                    <div class="shadowBox">
                        <!-- content -->
                        <div class="profileContent p-2 p-lg-4">
                            <!-- row -->
                            <div class="row mb-4">
                                <div class="col-md-12">
                                    <h5 class="flight h5 border-bottom mb-3 pb-3"><span class="fbold fblue">Update KYC Documents</span></h5>
                                </div>                              
                            </div>
                            <!--/ row -->
                             <div class="row">
                                            <!-- col -->
                                            <div class="col-md-6">
                                                <div class="downloadItem d-flex justify-content-between">
                                                    <div class="align-self-center">
                                                        <span class="icon-document icomoon h4"></span>
                                                        <span class="d-inline-block ps-3 titlename">Voter ID/Card</span>
                                                    </div>
                                                    <a href="javascript:void(0)" class="downIcon"><span class="icon-upload icomoon"></span></a>
                                                </div>
                                            </div>
                                            <!--/ col -->
                                             <!-- col -->
                                            <div class="col-md-6">
                                                <div class="downloadItem d-flex justify-content-between">
                                                    <div class="align-self-center">
                                                         <span class="icon-document icomoon h4"></span>
                                                        <span class="d-inline-block ps-3 titlename">PAN Copy</span>
                                                    </div>
                                                    <a href="javascript:void(0)" class="downIcon"><span class="icon-upload icomoon"></span></a>
                                                </div>
                                            </div>
                                            <!--/ col -->
                                             <!-- col -->
                                            <div class="col-md-6">
                                                <div class="downloadItem d-flex justify-content-between">
                                                    <div class="align-self-center">
                                                          <span class="icon-document icomoon h4"></span>
                                                        <span class="d-inline-block ps-3 titlename">Aadhar Card</span>
                                                    </div>
                                                    <a href="javascript:void(0)" class="downIcon"><span class="icon-upload icomoon"></span></a>
                                                </div>
                                            </div>
                                            <!--/ col -->
                                             <!-- col -->
                                            <div class="col-md-6">
                                                <div class="downloadItem d-flex justify-content-between">
                                                    <div class="align-self-center">
                                                         <span class="icon-document icomoon h4"></span>
                                                        <span class="d-inline-block ps-3 titlename">Ration Card</span>
                                                    </div>
                                                    <a href="javascript:void(0)" class="downIcon"><span class="icon-upload icomoon"></span></a>
                                                </div>
                                            </div>
                                            <!--/ col -->
                                        </div>

                         
                           
                        </div>
                        <!--/ content -->
                    </div>
                </div>
                <!--/ right section -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->

    </main>
    <!--/ main --> 
    <?php include 'includes/footer.php' ?> 
    <?php include 'includes/scripts.php'?>
</body>

</html>